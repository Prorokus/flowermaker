stars
- Delay -
active: false
- Duration - 
lowMin: 0.0
lowMax: 0.0
- Count - 
min: 0
max: 2000
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 5.0
highMax: 5.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 5000.0
highMax: 6000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: square
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 480.0
highMax: 480.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 200.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 8.0
highMax: 15.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 180.0
highMax: 180.0
relative: true
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 0.72156864
colors1: 1.0
colors2: 0.99607843
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 7
scaling0: 0.0
scaling1: 0.49122807
scaling2: 1.0
scaling3: 0.24561404
scaling4: 1.0
scaling5: 0.49122807
scaling6: 0.0
timelineCount: 7
timeline0: 0.0
timeline1: 0.0890411
timeline2: 0.23972602
timeline3: 0.39726028
timeline4: 0.5958904
timeline5: 0.89726025
timeline6: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
- Image Path -
E:\work\Flower Maker\art-src\particles\test-texture.png
