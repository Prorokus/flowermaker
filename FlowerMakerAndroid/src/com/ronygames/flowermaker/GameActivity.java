package com.ronygames.flowermaker;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.casualWorkshop.CasualWorkshopGame;
import com.casualWorkshop.MainActivity;
import com.casualWorkshop.callbacks.BlockObjectType;
import com.casualWorkshop.enums.GameType;
import com.casualWorkshop.shop.SKU;
import com.casualWorkshop.utils.Const;
import com.crashlytics.android.Crashlytics;
import com.ronygames.flowermaker.callbacks.FlowersActionResolver;
import com.ronygames.flowermaker.enums.FlowerBlockType;

public class GameActivity extends MainActivity implements FlowersActionResolver{

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        showFBKey = true;
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
    }

    @Override
    protected CasualWorkshopGame getGame() {
        return new FlowerMakerGame(this);
    }

    @Override
    protected GameType initGameType() {
        return GameType.ANDROID;
    }

    @Override
    protected SKU initGameSKU() {
        return new FlowerSKU(this);
    }

    @Override
    protected boolean isGetSocializeEnabled() {
        return true;
    }

    @Override
    public boolean isObjectBlocked(BlockObjectType objectType) {
        if(objectType == FlowerBlockType.IS_FLOWER){
            return !isFlowersOwned();
        } else if(objectType == FlowerBlockType.IS_DECORATIONS){
            return !isDecorationsOwned();
        }
        return false;
    }


    @Override
    public void showWantToBuyDialog(final BlockObjectType objectType, final Runnable callback) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(GameActivity.this, com.casualWorkshop.R.style.Theme_CustomDialog);
                dialog.setContentView(com.casualWorkshop.R.layout.buy_dialog);

                TextView buyAll = (TextView) dialog.findViewById(com.casualWorkshop.R.id.buyAll);
                buyAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        purchaseAll(callback);
                        dialog.dismiss();
                    }
                });


                String buyText = null;
                TextView buyType = (TextView) dialog.findViewById(com.casualWorkshop.R.id.buyType);

                switch (objectType.getId()){
                    case 0:
                        buyText = getString(R.string.buy_flowers);

                        buyType.setText(buyText);
                        buyType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                purchaseFlowers(callback);
                                dialog.dismiss();
                            }
                        });

                        break;
                    case 1:
                        buyText = getString(R.string.buy_decorations);

                        buyType.setText(buyText);
                        buyType.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                purchaseDecorations(callback);
                                dialog.dismiss();
                            }
                        });

                        break;
                }

                TextView buyNotNow = (TextView) dialog.findViewById(com.casualWorkshop.R.id.buyNotNow);
                buyNotNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                try{
                    if(!isFinishing()){
                        dialog.show();
                    }
                } catch (Exception e){
                }
            }
        });
    }

    @Override
    public boolean isAdMobVisible(){
        return !isAllOwned() && !isDecorationsOwned() && !isFlowersOwned();
    }

    @Override
    public boolean isAllOwned() {
        return isPurchaseOwned(FlowerMakerPurchase.UNLOCK_ALL);
    }

    public boolean isDecorationsOwned() {
        return isAllOwned() || isPurchaseOwned(FlowerMakerPurchase.UNLOCK_DECORATIONS);
    }

    private boolean isFlowersOwned(){
        return isAllOwned() || isPurchaseOwned(FlowerMakerPurchase.UNLOCK_FLOWERS);
    }

    @Override
    protected void initChartboost() {
        super.initChartboost();

        if(chartboost != null){
            chartboost.cacheInterstitial("Final Screen");
        }
    }

    @Override
    public void showInterstitial(final int id) {
        // Show an interstitial
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isAdMobVisible()) {
                    if (chartboost != null) {
                        switch (id) {
                            case 0:
                                chartboost.showInterstitial("Final Screen");
                                break;
                        }
                    }
                }
            }
        });
    }

    @Override
    public void showMyBoquets() {
        super.showMyCakes();
    }

    @Override
    public void purchaseFlowers(Runnable callback) {
        purchaseItem(FlowerMakerPurchase.UNLOCK_FLOWERS, callback);
    }

    @Override
    public void purchaseAll(final Runnable callback) {
        purchaseItem(FlowerMakerPurchase.UNLOCK_ALL, callback);
    }

    @Override
    public void purchaseDecorations(final Runnable callback) {
        purchaseItem(FlowerMakerPurchase.UNLOCK_DECORATIONS, callback);
    }
    
	@Override
	public void showRateApp(final IOnRate callback) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final Dialog dialog = new Dialog(GameActivity.this,
						R.style.Theme_CustomDialog);
				dialog.setContentView(R.layout.rate_dialog);

				TextView yesButton = (TextView) dialog.findViewById(R.id.yes);
				yesButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						editor.putBoolean(Const.SHOW_RATE_DIALOG, false);
						editor.putBoolean(Const.SHOW_UNLOCK_FREE_CAKE, true);
						editor.commit();

						String appPackageName = getPackageName();

						try {
							Intent marketIntent = new Intent(
									Intent.ACTION_VIEW, Uri
											.parse("market://details?id="
													+ appPackageName));
							marketIntent
									.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
											| Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
							startActivity(marketIntent);
						} catch (ActivityNotFoundException e) {
							try {
								Intent marketIntent = new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("http://www.amazon.com/gp/mas/dl/andro"
												+ "id?p=" + appPackageName));
								marketIntent
										.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
												| Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
								startActivity(marketIntent);
							} catch (ActivityNotFoundException x) {
								Toast.makeText(GameActivity.this, x.toString(),
										Toast.LENGTH_SHORT).show();
							}
						}

						if (callback != null) {
							callback.onRate();
						}

						dialog.dismiss();
					}
				});

				TextView noButton = (TextView) dialog.findViewById(R.id.no);
				noButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						editor.putBoolean(Const.SHOW_RATE_DIALOG, false);
						editor.commit();

						dialog.dismiss();
					}
				});

				TextView laterButton = (TextView) dialog
						.findViewById(R.id.later);
				laterButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});

				try {
					if (!isFinishing()) {
						dialog.show();
					}
				} catch (Exception e) {
				}
			}
		});
	}
}