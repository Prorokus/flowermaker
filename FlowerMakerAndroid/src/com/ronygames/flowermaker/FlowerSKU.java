package com.ronygames.flowermaker;

import com.casualWorkshop.MainActivity;
import com.casualWorkshop.shop.SKU;

public class FlowerSKU extends SKU {
    private final String ololoVerb2 = "nByEiuQYolUcVcUUBDj8Ix+dQY516EQ6mUfsuBbvCIhb3mzOckbT4mhW4HI2e4wkIVN94VEIM";
    private final String ololoVerb1 = "A2znzjRCipSDBtWDh1CQ/ZDEoGEQr3qkaRrWqxbUWFVNo1dCW";

    public FlowerSKU(MainActivity activity) {
        super(activity);
    }

    @Override
    public boolean needInitBaseIds() {
        return false;
    }

    @Override
    public void initIds() {
        addItem(FlowerMakerPurchase.UNLOCK_ALL);
        addItem(FlowerMakerPurchase.UNLOCK_FLOWERS);
        addItem(FlowerMakerPurchase.UNLOCK_DECORATIONS);
    }

    @Override
    public String getPS() {

        StringBuilder sb = new StringBuilder();
        sb.append("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArRDuTVot9FB5");
        sb.append("v4dz0WrtUENcnyBVtSfd5CLy4mHLSkyMJoCVeUSn1V0kg+u4yKxBV1tYEvAq3rXP7I");
        sb.append(ololoVerb1);
        sb.append(ololoVerb2);
        sb.append("5vQ7LpbaTCDej157rTQA6d5iMABCZOe9vKj7u7F7X/tl6y/MnXqmcxnuNXpJraKruofe83bMZpbs+GcN2pQf0n7iZw5VYiqBl4vAs/6UPd2b9f1rLwq/LGF4LC");
        sb.append("E4Yr8Vki6p5+yfRmEyVwIDAQAB");

        return sb.toString();
    }
}
