package com.ronygames.flowermaker;


import com.casualWorkshop.enums.PurchaseType;

/**
 * Created by vlad on 2/14/14.
 */
public enum FlowerMakerPurchase implements PurchaseType {
    UNLOCK_ALL("unlock_all"),
    UNLOCK_FLOWERS("unlock_flowers"),
    UNLOCK_DECORATIONS("unlock_decorations");

    private String key;

    FlowerMakerPurchase(String key){
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }
}
