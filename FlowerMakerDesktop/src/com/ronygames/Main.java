package com.ronygames;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ronygames.flowermaker.FlowerMakerGame;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "FlowerMaker";
        cfg.width = 480;
        cfg.height = 854;

		new LwjglApplication(new FlowerMakerGame(null), cfg);
	}
}
