package com.ronygames.flowermaker.objects;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.objects.GameObject;
import com.ronygames.flowermaker.callbacks.BeetleClickCallback;

import java.util.Random;

public class BeetleObject extends Group {
	private Random rand = new Random();
	private GameObject beetle1;
	
	private float startScale = 1.0f;
	
	public void resetBeelte()
	{
		beetle1.addAction(Actions.fadeIn(0.5f));
		beetle1.setTouchable(Touchable.disabled);
	}

	public void activateBeelte()
	{
		beetle1.setTouchable(Touchable.enabled);
	}

	public BeetleObject(String name, float y, boolean isMovingRight, final BeetleClickCallback callback)
	{
		super();
		setName(name);
		
        // ----------BEETLE1---------------
        ClickListener BeetleClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.getTarget().setTouchable(Touchable.disabled);
                event.getTarget().addAction(Actions.fadeOut(0.5f));
                callback.onBeetleClick();
                
                AudioPlayer.playSound("squeeze", 0.05f);
                
                return true;
            }
        };
        
        startScale = 0.75f + rand.nextFloat() / 4;
		
        beetle1 = new GameObject("beetle1", false, true);
        beetle1.skinName = "beetle";
        beetle1.setOrigin(38, 28);
        beetle1.setScale(startScale);
        beetle1.addAction(Actions.forever(Actions.sequence(Actions.moveBy(0, -10, 1.0f, Interpolation.sine), Actions.moveBy(0, 10, 1.0f, Interpolation.sine))));
        beetle1.folderName = "protectMg";
        beetle1.setTouchable(Touchable.enabled);
        beetle1.addListener(BeetleClickListener);
        this.addActor(beetle1);
        
        GameObject beetle1_wing_r = new GameObject("beetle-wing-r", false, true);
        beetle1_wing_r.setPositionXY(27, 47);
        beetle1_wing_r.setOrigin(7, 0);
        beetle1_wing_r.skinName = "beetle-wing-r";
        beetle1_wing_r.folderName = "protectMg";
        beetle1_wing_r.addAction(Actions.forever(Actions.sequence(Actions.rotateTo(15, 0.035f), Actions.rotateTo(0, 0.025f))));
        beetle1.addActor(beetle1_wing_r);     
        
        GameObject beetle1_wing_l = new GameObject("beetle-wing-l", false, true);
        beetle1_wing_l.setPositionXY(-3, 45);
        beetle1_wing_l.setOrigin(35, 0);
        beetle1_wing_l.skinName = "beetle-wing-l";
        beetle1_wing_l.folderName = "protectMg";
        beetle1_wing_l.addAction(Actions.forever(Actions.sequence(Actions.rotateTo(-15, 0.035f), Actions.rotateTo(0, 0.025f))));
        beetle1.addActor(beetle1_wing_l);          
        // -------------------------------------------
        
        if (isMovingRight)
        {
        	beetle1.setPositionXY(-100, y);
        	beetle1.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(startScale + 0.02f, startScale + 0.02f, 1.0f, Interpolation.sine), Actions.scaleTo(startScale - 0.02f, startScale - 0.02f, 1.0f, Interpolation.sine))));
        	beetle1.addAction(Actions.sequence(Actions.delay(rand.nextInt(10), Actions.forever(Actions.sequence(Actions.moveBy(580, 0, 5 + rand.nextInt(3)), Actions.moveBy(-580, 0, -0.0f))))));
        }
        else
        {
        	beetle1.setPositionXY(490, y);
        	beetle1.setScale(-startScale, startScale);
        	beetle1.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(-startScale - 0.02f, startScale + 0.02f, 1.0f, Interpolation.sine), Actions.scaleTo(-startScale + 0.02f, startScale - 0.02f, 1.0f, Interpolation.sine))));
        	beetle1.addAction(Actions.sequence(Actions.delay(rand.nextInt(10), Actions.forever(Actions.sequence(Actions.moveBy(-590, 0, 5 + rand.nextInt(3)), Actions.moveBy(590, 0, -0.0f))))));
        }
	}
}
