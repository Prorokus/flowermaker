package com.ronygames.flowermaker.helpers;

/**
 * Created by VLaD on 06.03.14.
 */
public class Const {
    public static final String TXT_DRAG_OVER_FLOWER = "Drag water over the flower to grow it.";
    public static final String TXT_DRAG_OVER_FLOWER2 = "Drag fertilizer over the flower to grow it.";

    public static final String TXT_DRAG_OVER_BEETLE1 = "Tap more ";
    public static final String TXT_DRAG_OVER_BEETLE2 = " beetles to finish";
}
