package com.ronygames.flowermaker;

import com.casualWorkshop.CasualWorkshopGame;
import com.casualWorkshop.enums.SChangeType;
import com.casualWorkshop.enums.ScreenOrientation;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.helpers.FontHelper;
import com.casualWorkshop.helpers.ImageCache;
import com.ronygames.flowermaker.callbacks.FlowersActionResolver;
import com.ronygames.flowermaker.screens.*;

public class FlowerMakerGame extends CasualWorkshopGame {

    public FlowerMakerGame(FlowersActionResolver actionResolver) {
        super(actionResolver);

        Constants.SAVE_FOLDER = "/FlowerMaker";
    }

    @Override
    protected void loadScene(){
    	super.loadScene();
//        mScreenChangeHelper.changeScene("makeWrap");
    }

    @Override
    protected void initFonts() {
        fontHelper = new FontHelper() {
            @Override
            public void initFonts() {
                addFont("SegoePrint-32-Shadow");
            }
        };
    }

    @Override
    protected void initScreens() {
        gameScreens.add(new MainMenuScreen(this, "mainMenu", mActionResolver));
        gameScreens.add(new ShopScreen(this, "shop", mActionResolver));
        gameScreens.add(new GrowFlowersScreen(this, "growFlowers", mActionResolver));
        gameScreens.add(new ProtectMgScreen(this, "protectMg", mActionResolver));
        gameScreens.add(new MakeWrapScreen(this, "makeWrap", mActionResolver));
        gameScreens.add(new MakeBouquetScreen(this, "makeBouquet", mActionResolver));
        gameScreens.add(new FinalScene(this, "final", mActionResolver));
    }

    @Override
    protected void loadSounds() {
    }

    @Override
    protected ScreenOrientation getScreenOrientation() {
        return ScreenOrientation.PORTRAIT;
    }

    @Override
    protected void initImageCache(){
        ImageCache.load("main");
        ImageCache.load("particles");
    }

    @Override
    protected SChangeType getScreenChangeType(){
        return SChangeType.FADE;
    }

}

