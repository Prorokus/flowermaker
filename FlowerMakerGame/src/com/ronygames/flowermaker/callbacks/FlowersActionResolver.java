package com.ronygames.flowermaker.callbacks;

import com.casualWorkshop.callbacks.ActionResolver;

/**
 * Created by VLaD on 06.03.14.
 */
public interface FlowersActionResolver extends ActionResolver {
    public void showMyBoquets();
    public void purchaseFlowers(final Runnable callback);
    public void purchaseAll(final Runnable callback);
    public void purchaseDecorations(final Runnable callback);
}
