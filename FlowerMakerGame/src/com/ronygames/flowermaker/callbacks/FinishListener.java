package com.ronygames.flowermaker.callbacks;

/**
 * Created by VLaD on 04.01.14.
 */
public interface FinishListener {
    public void onFinish();
}
