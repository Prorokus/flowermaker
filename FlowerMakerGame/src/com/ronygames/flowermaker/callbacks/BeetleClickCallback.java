package com.ronygames.flowermaker.callbacks;

/**
 * Created by VLaD on 27.12.13.
 */
public interface BeetleClickCallback {
    public void onBeetleClick();
}
