package com.ronygames.flowermaker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.casualWorkshop.CasualWorkshopGame;
import com.casualWorkshop.callbacks.ActionResolver;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.objects.GameObject;
import com.casualWorkshop.screens.AbstractScreen;
import com.ronygames.flowermaker.FlowerMakerGame;
import com.ronygames.flowermaker.callbacks.FlowersActionResolver;

public class MainMenuScreen extends AbstractScreen {
	
	private GameObject playButton;
	private GameObject shop;
	private GameObject myBoquets;
	private GameObject moreGames;

    public int getScreenNumb() {
        return 1;
    }

    public MainMenuScreen(Game game, String name, ActionResolver actionResolver) {
        super(game, name, actionResolver);

        // ----------BACKGROUND---------------
        GameObject background = new GameObject("background", false, false);
        background.setPositionXY(0, 0);
        background.folderName = "mainMenu";
        background.setOrigin(240, 754);
        background.createParticle("stars", true);
        rootGroup.addActor(background);
        // -------------------------------------------

        // ----------CLOUDS1---------------
        GameObject clouds1 = new GameObject("clouds", false, true);
        clouds1.setPositionXY(14, 395);
        clouds1.setOrigin(241, 222);
        clouds1.getColor().a = 0.7f;
        clouds1.setRotation(30);
        clouds1.addAction(Actions.forever(Actions.rotateBy(-1, 0.75f)));
        clouds1.folderName = "main";
        rootGroup.addActor(clouds1);
        // -------------------------------------------

        // ----------SUN---------------
        GameObject sun = new GameObject("sun", false, true);
        sun.setPositionXY(116, 500);
        sun.setColor(1, 1, 0.9f, 1);
        sun.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 0.9f, 0.9f, 0.9f), 10, Interpolation.sine), Actions.color(new Color(1, 1, 0.9f, 1), 10, Interpolation.sine))));
        sun.folderName = "main";
        rootGroup.addActor(sun);
        // -------------------------------------------

        // ----------GROUND---------------
        GameObject ground = new GameObject("ground", false, true);
        ground.setPositionXY(0, 0);
        ground.folderName = "mainMenu";
        rootGroup.addActor(ground);
        // -------------------------------------------
        
        // ----------BUSH_RIGHT---------------
        GameObject bush1 = new GameObject("bush1", false, true);
        bush1.setPositionXY(240, -88);
        bush1.setOrigin(244, 50);
        bush1.addAction(Actions.forever(Actions.sequence(Actions.delay(5), Actions.rotateTo(-5, 0.75f, Interpolation.sine), Actions.delay(2), Actions.rotateTo(0, 0.75f, Interpolation.sine))));
        bush1.addAction(Actions.forever(Actions.sequence(Actions.delay(5), Actions.moveBy(15, -25, 0.75f, Interpolation.sine), Actions.delay(2), Actions.moveBy(-15, 25, 0.75f, Interpolation.sine))));
        bush1.folderName = "mainMenu";
        rootGroup.addActor(bush1);
        // -------------------------------------------
        
        // ----------BUSH LEFT---------------
        GameObject bush2 = new GameObject("bush2", false, true);
        bush2.setPositionXY(-5, -18);
        bush2.setOrigin(0, 0);
        bush2.addAction(Actions.forever(Actions.sequence(Actions.delay(5), Actions.rotateTo(5, 0.75f, Interpolation.sine), Actions.delay(2), Actions.rotateTo(0, 0.75f, Interpolation.sine))));
        bush2.addAction(Actions.forever(Actions.sequence(Actions.delay(5), Actions.moveBy(-15, -25, 0.75f, Interpolation.sine), Actions.delay(2), Actions.moveBy(15, 25, 0.75f, Interpolation.sine))));
        bush2.addAction(Actions.forever(Actions.sequence()));
        bush2.folderName = "mainMenu";
        rootGroup.addActor(bush2);
        // -------------------------------------------

        // ----------FENCE_LEFT---------------
        GameObject fenceL = new GameObject("fence-l", false, true);
        fenceL.setPositionXY(-70, 275);
        fenceL.folderName = "mainMenu";
        rootGroup.addActor(fenceL);
        // -------------------------------------------

        // ----------FENCE_RIGHT---------------
        GameObject fenceR = new GameObject("fence-r", false, true);
        fenceR.setPositionXY(330, 260);
        fenceR.folderName = "mainMenu";
        rootGroup.addActor(fenceR);
        // -------------------------------------------

        // ----------DECOR_LEFT---------------
        GameObject decorL = new GameObject("flowers-decor-l", false, true);
        decorL.setPositionXY(-180, 190);
        decorL.folderName = "mainMenu";
        decorL.setOrigin(150, 75);
        decorL.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(1.01f, 0.98f, 1.0f, Interpolation.sine), Actions.scaleTo(1.0f, 1.0f, 1.0f, Interpolation.sine))));
        rootGroup.addActor(decorL);
        // -------------------------------------------
        
        // ----------DECOR_RIGHT---------------
        GameObject decorR = new GameObject("flowers-decor-r", false, true);
        decorR.setPositionXY(340, 180);
        decorR.setOrigin(150, 75);
        decorR.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(1.01f, 0.98f, 1.0f, Interpolation.sine), Actions.scaleTo(1.0f, 1.0f, 1.0f, Interpolation.sine))));
        decorR.folderName = "mainMenu";
        rootGroup.addActor(decorR);
        // -------------------------------------------

        // ----------BUTTON PLAY---------------
        ClickListener playClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                if (mActionResolver != null) {
                    mActionResolver.incrementPlays();
                }

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("growFlowers");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };

        playButton = new GameObject("pla-btn", false, true);
        playButton.setPositionXY(152, 294);
        playButton.folderName = "mainMenu";
        playButton.setOrigin(89, 41);
        playButton.setTouchable(Touchable.enabled);
        playButton.addListener(playClickListener);
        playButton.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(1.025f, 1.025f, 0.5f, Interpolation.sine), Actions.scaleTo(0.975f, 0.975f, 0.5f, Interpolation.sine))));
        rootGroup.addActor(playButton);
        // -------------------------------------------

        // ----------BUTTON_MORE_GAMES----------------
        ClickListener moreGamesClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                if (mActionResolver != null) {
                    mActionResolver.showMoreGames();
                }

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };

        moreGames = new GameObject("more-games-btn", false, true);
        moreGames.setPositionXY(299, 21);
        moreGames.folderName = "mainMenu";
        moreGames.setTouchable(Touchable.enabled);
        moreGames.addListener(moreGamesClickListener);
        rootGroup.addActor(moreGames);
        // -------------------------------------------

        // ----------BUTTON_MY_BOQUETS----------------
        ClickListener myBoquetsClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                if (mActionResolver != null) {
                    ((FlowersActionResolver)mActionResolver).showMyBoquets();
                }

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };

        myBoquets = new GameObject("my-boquets-btn", false, true);
        myBoquets.setPositionXY(68, 174);
        myBoquets.folderName = "mainMenu";
        myBoquets.setTouchable(Touchable.enabled);
        myBoquets.addListener(myBoquetsClickListener);
        rootGroup.addActor(myBoquets);

        // -------------------------------------------

        // ----------BUTTON_SHOP----------------
        ClickListener shopClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("shop");

                event.getTarget().clearActions();
                event.getTarget().setColor(1, 1, 1, 1);
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };

        shop = new GameObject("shop-btn", false, true);
        shop.setPositionXY(14, 21);
        shop.folderName = "mainMenu";
        shop.setTouchable(Touchable.enabled);
        shop.addListener(shopClickListener);
        rootGroup.addActor(shop);
        // -------------------------------------------

        FlowerMakerGame.gameStage.addActor(rootGroup);
    }

    @Override
    public void render(float delta) {
        clipBounds.set(rootGroup.getX(), rootGroup.getY(), CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT);

        ScissorStack.calculateScissors(camera, CasualWorkshopGame.START_X, CasualWorkshopGame.START_Y, CasualWorkshopGame.SCREEN_WIDTH, CasualWorkshopGame.SCREEN_HEIGHT, spriteBatch.getTransformMatrix(), clipBounds, scissors);
        boolean isPushed = ScissorStack.pushScissors(scissors);

        spriteBatch.begin();
        rootGroup.draw(spriteBatch, 1);
        spriteBatch.end();

        if(isPushed){
            ScissorStack.popScissors();
        }
        
        rootGroup.act(delta);

        if (!firstEnterFrame && Gdx.input.isKeyPressed(Keys.BACK) && rootGroup.getActions().size == 0) 
        {
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            FlowerMakerGame.mScreenChangeHelper.resetLoadingThread();

            dispose();
            Gdx.app.exit();
        }

        if (firstEnterFrame && !Gdx.input.isKeyPressed(Keys.BACK)) {
            firstEnterFrame = false;
        }
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        firstEnterFrame = true;

        AudioPlayer.playMusic("backgroundMusic", 0.25f);

        hideAds();

        resetVariables();
    }

    @Override
    public void notifyObjectsOnSceneShow() {
        notifyObjectsOnSceneShow(rootGroup);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
    	super.resume();
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreUnLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScenePlaced() {
    }

    @Override
    protected void resetVariables() {
    	playButton.clearActions();
    	playButton.setScale(1);
    	playButton.setColor(1, 1, 1, 1);
    	playButton.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(1.025f, 1.025f, 0.5f, Interpolation.sine), Actions.scaleTo(0.975f, 0.975f, 0.5f, Interpolation.sine))));
    	
    	shop.clearActions();
    	shop.setColor(1, 1, 1, 1);

    	myBoquets.clearActions();
    	myBoquets.setColor(1, 1, 1, 1);

    	moreGames.clearActions();
    	moreGames.setColor(1, 1, 1, 1);
    }

    @Override
    protected String getPrevScreenName() {
        return null;
    }
}
