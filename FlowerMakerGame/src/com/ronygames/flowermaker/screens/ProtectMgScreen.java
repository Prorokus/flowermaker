package com.ronygames.flowermaker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.casualWorkshop.callbacks.ActionResolver;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.objects.GameObject;
import com.casualWorkshop.objects.TextGameObject;
import com.casualWorkshop.screens.AbstractScreen;
import com.ronygames.flowermaker.FlowerMakerGame;
import com.ronygames.flowermaker.callbacks.BeetleClickCallback;
import com.ronygames.flowermaker.helpers.Const;
import com.ronygames.flowermaker.objects.BeetleObject;

public class ProtectMgScreen extends AbstractScreen implements BeetleClickCallback{

    private GameObject home_btn;

	private GameObject next_btn;
	private GameObject insecticide;
	private GameObject insecticideEffect;
	private GameObject swatter;
	private GameObject secateurs;
	private GameObject glove;
	private GameObject flower;
	
	private BeetleObject bObj1;
	private BeetleObject bObj2;
	private BeetleObject bObj3;
	private BeetleObject bObj4;
	private BeetleObject bObj5;
	private BeetleObject bObj6;
	private BeetleObject bObj7;
	private BeetleObject bObj8;
	private BeetleObject bObj9;
	
	private int smashedBeetleCount = 0;
	
	private TextGameObject tapBeetleText;
	
    private enum ProtectMgState{
        CHOOSE,
        SMASH,
        CUT,
        GRAB,
        FINISHED
    }
    
    private ProtectMgState mProtectMgState = ProtectMgState.CHOOSE;

    public int getScreenNumb() {
        return 3;
    }

    public Group getRootGroup() {
        return rootGroup;
    }

    public ProtectMgScreen(Game game, String name, ActionResolver actionResolver) {
        super(game, name, actionResolver);

        // ----------BACKGROUND---------------
        GameObject background = new GameObject("background", false, false);
        background.setPositionXY(0, 0);
        background.folderName = "main";
        background.setOrigin(240, 754);
        background.createParticle("stars", true);
        rootGroup.addActor(background);
        // -------------------------------------------
        
        // ----------CLOUDS1---------------
        GameObject clouds1 = new GameObject("clouds", false, true);
        clouds1.setPositionXY(-10, 360);
        clouds1.setOrigin(241, 222);
        clouds1.getColor().a = 0.7f;
        clouds1.setScale(1.33f);
        clouds1.setRotation(30);
        clouds1.addAction(Actions.forever(Actions.rotateBy(-1, 0.75f)));
        clouds1.folderName = "main";
        rootGroup.addActor(clouds1);
        // -------------------------------------------
        
        // ----------SUN---------------
        GameObject sun = new GameObject("sun", false, true);
        sun.setPositionXY(41, 416);
        sun.setColor(1, 1, 0.9f, 1);
        sun.setScale(1.33f);
        sun.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 0.9f, 0.9f, 0.9f), 10, Interpolation.sine), Actions.color(new Color(1, 1, 0.9f, 1), 10, Interpolation.sine))));
        sun.folderName = "main";
        rootGroup.addActor(sun);
        // -------------------------------------------
        
        // ----------GROUND---------------
        GameObject ground = new GameObject("ground", false, true);
        ground.setPositionXY(0, 0);
        ground.folderName = "protectMg";
        rootGroup.addActor(ground);
        // -------------------------------------------

        // ----------FRONT---------------
        GameObject front = new GameObject("front", false, true);
        front.setPositionXY(-40, -10);
        front.folderName = "protectMg";
        rootGroup.addActor(front);
        // -------------------------------------------

        // ----------FLOWER---------------
        flower = new GameObject("flower", false, true);
        flower.setPositionXY(140, 268);
        flower.setOrigin(114, 51);
        flower.setScale(1.01f, 0.98f);
        flower.setRotation(-1);
        flower.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(0.98f, 1.01f, 1.0f, Interpolation.sine), Actions.scaleTo(1.01f, 0.98f, 1.0f, Interpolation.sine))));
        flower.addAction(Actions.forever(Actions.sequence(Actions.rotateTo(1, 2.0f, Interpolation.sine), Actions.rotateTo(-1, 2.0f, Interpolation.sine))));
        flower.folderName = "protectMg";
        rootGroup.addActor(flower);
        // -------------------------------------------
        
        // ----------INSECTICIDE---------------
        ClickListener insecticideClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound01", 0.25f);
        		mProtectMgState = ProtectMgState.SMASH;
        		
        		insecticide.clearParticles();
        		insecticide.setTouchable(Touchable.disabled);
                insecticide.setOrigin(66, 10);
                insecticide.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(0.98f, 1.01f, 1.0f, Interpolation.sine), Actions.scaleTo(1.01f, 0.98f, 1.0f, Interpolation.sine))));
                insecticide.addAction(Actions.forever(Actions.sequence(Actions.rotateTo(1, 2.0f, Interpolation.sine), Actions.rotateTo(-1, 2.0f, Interpolation.sine))));
                
                insecticideEffect.createParticle("airWater", true);
                
                swatter.clearParticles();
                swatter.setTouchable(Touchable.disabled);
                swatter.addAction(Actions.fadeOut(0.25f));
                
        		bObj1.activateBeelte();
        		bObj2.activateBeelte();
        		bObj3.activateBeelte();
        		bObj4.activateBeelte();
        		bObj5.activateBeelte();
        		bObj6.activateBeelte();
        		bObj7.activateBeelte();
        		bObj8.activateBeelte();
        		bObj9.activateBeelte();

        		tapBeetleText.addAction(Actions.fadeIn(0.25f));
        		
                return true;
            }
        };
        
        insecticide = new GameObject("insecticide", false, true);
        insecticide.setPositionXY(369, 215);
        insecticide.folderName = "protectMg";
        insecticide.setOrigin(56, 93);
        insecticide.createParticle("useArea", true);
        insecticide.addListener(insecticideClickListener);
        rootGroup.addActor(insecticide);
        
        insecticideEffect = new GameObject("insecticideEffect", false, true);
        insecticideEffect.setPositionXY(0, 0);
        insecticideEffect.skinName = "";
        insecticideEffect.folderName = "protectMg";
        insecticideEffect.setOrigin(18, 172);
        insecticide.addActor(insecticideEffect);
        // -------------------------------------------
        
        // ----------SWATTER---------------
        ClickListener swatterClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound01", 0.25f);
        		mProtectMgState = ProtectMgState.SMASH;
        		
        		swatter.clearParticles();
        		swatter.setTouchable(Touchable.disabled);
        		swatter.setOrigin(10, 10);
        		swatter.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(0.98f, 1.01f, 1.0f, Interpolation.sine), Actions.scaleTo(1.01f, 0.98f, 1.0f, Interpolation.sine))));
        		swatter.addAction(Actions.forever(Actions.sequence(Actions.rotateTo(1, 2.0f, Interpolation.sine), Actions.rotateTo(-1, 2.0f, Interpolation.sine))));
                
                insecticide.clearParticles();
                insecticide.setTouchable(Touchable.disabled);
                insecticide.addAction(Actions.fadeOut(0.25f));
                
        		bObj1.activateBeelte();
        		bObj2.activateBeelte();
        		bObj3.activateBeelte();
        		bObj4.activateBeelte();
        		bObj5.activateBeelte();
        		bObj6.activateBeelte();
        		bObj7.activateBeelte();
        		bObj8.activateBeelte();
        		bObj9.activateBeelte();
        		
        		tapBeetleText.addAction(Actions.fadeIn(0.25f));

                return true;
            }
        };
        
        swatter = new GameObject("swatter", false, true);
        swatter.setPositionXY(10, 199);
        swatter.folderName = "protectMg";
        swatter.setOrigin(63, 118);
        swatter.createParticle("useArea", true);
        swatter.addListener(swatterClickListener);
        rootGroup.addActor(swatter);
        // -------------------------------------------
        
        // ----------SECATEURS---------------
        ClickListener secateursClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound01", 0.25f);
        		secateurs.setTouchable(Touchable.disabled);
        		secateurs.clearParticles();
        		
        		secateurs.addAction(Actions.sequence(Actions.moveTo(260, 220, 1.0f, Interpolation.sine), Actions.fadeOut(0.25f), Actions.run(new Runnable() {
					@Override
					public void run() {
		        		mProtectMgState = ProtectMgState.GRAB;
		        		glove.addAction(Actions.fadeIn(0.25f));
		        		glove.setTouchable(Touchable.enabled);
		        		glove.createParticle("useArea", true);
		        		
		        		flower.clearActions();
		        		flower.setRotation(75);
		        		flower.setPosition(220, 260);
		        		flower.setScale(1);
					}
				})));
        		secateurs.addAction(Actions.sequence(Actions.rotateTo(30, 1.0f, Interpolation.sine)));
        		
                return true;
            }
        };
        
        secateurs = new GameObject("secateurs", false, true);
        secateurs.setPositionXY(90, 142);
        secateurs.folderName = "protectMg";
        secateurs.setColor(1, 1, 1, 0);
        secateurs.setOrigin(55, 68);
        secateurs.addListener(secateursClickListener);
        rootGroup.addActor(secateurs);
        // -------------------------------------------
        
        // ----------GLOVE---------------
        ClickListener gloveClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound01", 0.25f);
            	glove.setTouchable(Touchable.disabled);
            	glove.clearParticles();
        		
            	glove.addAction(Actions.sequence(Actions.moveTo(260, 220, 1.0f, Interpolation.sine), Actions.fadeOut(0.25f), Actions.run(new Runnable() {
					@Override
					public void run() {
		        		mProtectMgState = ProtectMgState.FINISHED;
		        		next_btn.addAction(Actions.sequence(Actions.scaleTo(1.2f, 1.2f, 0.25f, Interpolation.sine), Actions.scaleTo(1.0f, 1.0f, 0.25f, Interpolation.sine)));
		        		next_btn.setVisible(true);
		        		next_btn.setTouchable(Touchable.enabled);
		        		next_btn.createParticle("useArea", true);
		        		
		        		flower.addAction(Actions.fadeOut(0.25f));
					}
				})));
        		
            	glove.addAction(Actions.sequence(Actions.rotateTo(30, 1.0f, Interpolation.sine)));
        		
                return true;
            }
        };
        
        glove = new GameObject("glove", false, true);
        glove.setPositionXY(209, 150);
        glove.setColor(1, 1, 1, 0);
        glove.setOrigin(80, 71);
        glove.folderName = "protectMg";
        glove.addListener(gloveClickListener);
        rootGroup.addActor(glove);
        // -------------------------------------------

        // -------------------------------------------
        tapBeetleText = new TextGameObject("tapBeetleText", false, "SegoePrint-32-Shadow", 32);
        tapBeetleText.setTextCenteredOnPosition(Const.TXT_DRAG_OVER_BEETLE1 + "9" + Const.TXT_DRAG_OVER_BEETLE2, 240, 654);
        tapBeetleText.getColor().a = 0;
        rootGroup.addActor(tapBeetleText);
        // -------------------------------------------

        //-------------BEETLES----------------
        bObj1 = new BeetleObject("beetle1", 450, true, this);
        rootGroup.addActor(bObj1);

        bObj2 = new BeetleObject("beetle1", 500, true, this);
        rootGroup.addActor(bObj2);

        bObj3 = new BeetleObject("beetle1", 550, false, this);
        rootGroup.addActor(bObj3);

        bObj4 = new BeetleObject("beetle1", 600, false, this);
        rootGroup.addActor(bObj4);

        bObj5 = new BeetleObject("beetle1", 650, true, this);
        rootGroup.addActor(bObj5);

        bObj6 = new BeetleObject("beetle1", 500, false, this);
        rootGroup.addActor(bObj6);

        bObj7 = new BeetleObject("beetle1", 600, true, this);
        rootGroup.addActor(bObj7);
        
        bObj8 = new BeetleObject("beetle1", 450, true, this);
        rootGroup.addActor(bObj8);

        bObj9 = new BeetleObject("beetle1", 400, false, this);
        rootGroup.addActor(bObj9);
        //------------------------------------
        
        // ----------HOME-BTN---------------
        ClickListener homeClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("mainMenu");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        home_btn = new GameObject("home-btn", false, true);
        home_btn.setPositionXY(355, 780);
        home_btn.folderName = "main";
        home_btn.setTouchable(Touchable.enabled);
        home_btn.addListener(homeClickListener);
        rootGroup.addActor(home_btn);
        // -------------------------------------------
        
        // ----------NEXT-BTN---------------
        ClickListener nextClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("makeWrap");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        next_btn = new GameObject("next-btn", false, true);
        next_btn.setPositionXY(355, 16);
        next_btn.setOrigin(55, 39);
        next_btn.folderName = "main";
        next_btn.setTouchable(Touchable.enabled);
        next_btn.addListener(nextClickListener);
        rootGroup.addActor(next_btn);
        // -------------------------------------------

        // ----------BACK-BTN---------------
        ClickListener backClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene(getPrevScreenName());

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        GameObject back_btn = new GameObject("back-btn", false, true);
        back_btn.setPositionXY(15, 16);
        back_btn.folderName = "main";
        back_btn.setTouchable(Touchable.enabled);
        back_btn.addListener(backClickListener);
        rootGroup.addActor(back_btn);
        // -------------------------------------------
        
        FlowerMakerGame.gameStage.addActor(rootGroup);
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreUnLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScenePlaced() {
        // TODO Auto-generated method stub

    }
    
	@Override
	protected void resetVariables() {
		flower.clearActions();
        flower.setPositionXY(140, 268);
        flower.setScale(1.01f, 0.98f);
        flower.setColor(1, 1, 1, 1);
        flower.setRotation(-1);
        flower.addAction(Actions.forever(Actions.sequence(Actions.scaleTo(0.98f, 1.01f, 1.0f, Interpolation.sine), Actions.scaleTo(1.01f, 0.98f, 1.0f, Interpolation.sine))));
        flower.addAction(Actions.forever(Actions.sequence(Actions.rotateTo(1, 2.0f, Interpolation.sine), Actions.rotateTo(-1, 2.0f, Interpolation.sine))));

        secateurs.setPositionXY(90, 142);
        secateurs.setRotation(0);
        secateurs.setColor(1, 1, 1, 0);
        secateurs.setTouchable(Touchable.disabled);

        glove.setPositionXY(90, 142);
        glove.setRotation(0);
        glove.setColor(1, 1, 1, 0);
        glove.setTouchable(Touchable.disabled);

		bObj1.resetBeelte();
		bObj2.resetBeelte();
		bObj3.resetBeelte();
		bObj4.resetBeelte();
		bObj5.resetBeelte();
		bObj6.resetBeelte();
		bObj7.resetBeelte();
		bObj8.resetBeelte();
		bObj9.resetBeelte();
		smashedBeetleCount = 0;
		
		mProtectMgState = ProtectMgState.CHOOSE;
		
		insecticide.clearActions();
		insecticide.setRotation(0);
		insecticide.setScale(1);
		insecticide.setColor(1, 1, 1, 1);
        insecticide.setOrigin(56, 93);
        insecticide.createParticle("useArea", true);
        insecticide.setTouchable(Touchable.enabled);
        insecticideEffect.clearParticles();

        swatter.clearActions();
		swatter.setRotation(0);
		swatter.setScale(1);
        swatter.setColor(1, 1, 1, 1);
        swatter.setOrigin(63, 118);
        swatter.createParticle("useArea", true);
        swatter.setTouchable(Touchable.enabled);
        
        tapBeetleText.setTextCenteredOnPosition(Const.TXT_DRAG_OVER_BEETLE1 + "9" + Const.TXT_DRAG_OVER_BEETLE2, 240, 654);
        tapBeetleText.setColor(1, 1, 1, 0);
        
        next_btn.setVisible(false);
        next_btn.clearParticles();
	}

	@Override
	protected String getPrevScreenName() {
		return "growFlowers";
	}

	@Override
	public void onBeetleClick() {
		smashedBeetleCount++;

        tapBeetleText.setTextCenteredOnPosition(Const.TXT_DRAG_OVER_BEETLE1 + String.valueOf(9 - smashedBeetleCount) + Const.TXT_DRAG_OVER_BEETLE2, 240, 654);

		if (smashedBeetleCount >= 9)
		{
			mProtectMgState = ProtectMgState.CUT;
			
			if (swatter.getColor().a > 0)
			{
                swatter.clearParticles();
                swatter.setTouchable(Touchable.disabled);
                swatter.addAction(Actions.fadeOut(0.25f));
			}
			
			if (insecticide.getColor().a > 0)
			{
				insecticide.clearParticles();
				insecticide.setTouchable(Touchable.disabled);
				insecticide.addAction(Actions.fadeOut(0.25f));
				insecticideEffect.clearParticles();
			}
			
			secateurs.setTouchable(Touchable.enabled);
			secateurs.addAction(Actions.fadeIn(0.25f));
			secateurs.createParticle("useArea", true);
			
			tapBeetleText.addAction(Actions.fadeOut(0.25f));
		}
	}

    @Override
    protected void onAdsLoad(int adsHeight){
        home_btn.setPositionY(780 - adsHeight);
    }
}
