package com.ronygames.flowermaker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.casualWorkshop.CasualWorkshopGame;
import com.casualWorkshop.callbacks.ActionResolver;
import com.casualWorkshop.callbacks.NavBarElemSelectedCallback;
import com.casualWorkshop.callbacks.PopUpElemSelectedCallback;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.objects.BlockedGameObject;
import com.casualWorkshop.objects.GameObject;
import com.casualWorkshop.objects.PopUpWindowSimple;
import com.casualWorkshop.screens.AbstractScreen;
import com.ronygames.flowermaker.FlowerMakerGame;
import com.ronygames.flowermaker.enums.FlowerBlockType;

public class MakeWrapScreen extends AbstractScreen implements PopUpElemSelectedCallback, NavBarElemSelectedCallback {

    private GameObject home_btn;

	private Color drawColor = null;
	
	private Color whiteColor = new Color(249.0f / 255.0f, 239.0f / 255.0f, 207.0f / 255.0f, 1.0f);
	private Color redColor = new Color(225.0f / 255.0f, 73.0f / 255.0f, 13.0f / 255.0f, 1.0f);
	private Color pinkColor = new Color(220.0f / 255.0f, 141.0f / 255.0f, 150.0f / 255.0f, 1.0f);
	private Color greenColor = new Color(125.0f / 255.0f, 177.0f / 255.0f, 71.0f / 255.0f, 1.0f);
	private Color blueColor = new Color(96.0f / 255.0f, 178.0f / 255.0f, 226.0f / 255.0f, 1.0f);
	private Color yellowColor = new Color(255.0f / 255.0f, 218.0f / 255.0f, 92.0f / 255.0f, 1.0f);
	
	private boolean isZoomInPressed = false;
	private boolean isZoomOutPressed = false;
	
	private Vector2 drawDragVec = new Vector2();
	private Vector2 lastDrawXY = new Vector2(0, 0);
	private Vector2 newDrawXY = new Vector2(0, 0);

	private GameObject draw_click_area;
	
	private PopUpWindowSimple pop_up_window;
	//private NavigationBarSimple navBarPanel;
	
	private GameObject background;
	private GameObject clouds1;
	
	private GameObject wrapObject;
	private GameObject wrapObjectEmpty;
	
	private GameObject pen;
	private GameObject boquet_color;
	private GameObject boquet_red;
	private GameObject boquet_yellow;
	private GameObject boquet_pink;
	private GameObject boquet_green;
	private GameObject boquet_blue;
	private GameObject boquet_white;
	
	private GameObject clear_btn;
	
	private Pixmap drawMask;
	private Pixmap renderPixmap64;
	private Pixmap drawPixmap;
	private Pixmap pixmapBuffer;

	private Texture drawTexture;
	private FrameBuffer fbo;
	
	private int lastModification = 0;
	private final PixmapChange[] modifications = new PixmapChange[50];

	private class PixmapChange {
		
		int x, y;
		int width;
	
		void set(int x, int y, int width) {
			this.x = x;
			this.y = y;
			this.width = width;
		}
	}
	
	public Pixmap getWrapPixmap()
	{
		return pixmapBuffer;
	}
	
	private void selectColor(String colorName)
	{
		boquet_color.clearActions();
		pen.clearActions();
		boquet_red.clearActions();
		boquet_yellow.clearActions();
		boquet_pink.clearActions();
		boquet_green.clearActions();		
		boquet_blue.clearActions();
		boquet_white.clearActions();
		
		if (drawColor == null && !colorName.equals(""))
		{
			boquet_red.setTouchable(Touchable.enabled);
			boquet_yellow.setTouchable(Touchable.enabled);
			boquet_pink.setTouchable(Touchable.enabled);
			boquet_green.setTouchable(Touchable.enabled);
			boquet_blue.setTouchable(Touchable.enabled);
			boquet_white.setTouchable(Touchable.enabled);
			
			boquet_color.getColor().a = 1;
			boquet_color.setScale(1);
			boquet_color.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));

			pen.getColor().a = 0;
			pen.setScale(0);
			pen.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));

			boquet_red.getColor().a = 0;
			boquet_red.setScale(0);
			boquet_red.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));

			boquet_yellow.getColor().a = 0;
			boquet_yellow.setScale(0);
			boquet_yellow.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));

			boquet_pink.getColor().a = 0;
			boquet_pink.setScale(0);
			boquet_pink.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));

			boquet_green.getColor().a = 0;
			boquet_green.setScale(0);
			boquet_green.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));

			boquet_blue.getColor().a = 0;
			boquet_blue.setScale(0);
			boquet_blue.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));

			boquet_white.getColor().a = 0;
			boquet_white.setScale(0);
			boquet_white.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));			
		}
		
		boquet_red.setUseLightenFade(false);
		boquet_red.setColor(1, 1, 1, 1);
		
		boquet_yellow.setUseLightenFade(false);
		boquet_yellow.setColor(1, 1, 1, 1);

		boquet_pink.setUseLightenFade(false);
		boquet_pink.setColor(1, 1, 1, 1);

		boquet_green.setUseLightenFade(false);
		boquet_green.setColor(1, 1, 1, 1);

		boquet_blue.setUseLightenFade(false);
		boquet_blue.setColor(1, 1, 1, 1);

		boquet_white.setUseLightenFade(false);
		boquet_white.setColor(1, 1, 1, 1);
		
		if (colorName.equals("white"))
		{
			drawColor = whiteColor;
			boquet_white.setUseLightenFade(true);
			boquet_white.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 1, 1, 0), 0.25f, Interpolation.sine), Actions.color(new Color(1, 1, 1, 0.5f), 0.25f, Interpolation.sine))));
		}
		else if (colorName.equals("red"))
		{
			drawColor = redColor;
			boquet_red.setUseLightenFade(true);
			boquet_red.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 1, 1, 0), 0.25f, Interpolation.sine), Actions.color(new Color(1, 1, 1, 0.5f), 0.25f, Interpolation.sine))));
		}
		else if (colorName.equals("green"))
		{
			drawColor = greenColor;
			boquet_green.setUseLightenFade(true);
			boquet_green.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 1, 1, 0), 0.25f, Interpolation.sine), Actions.color(new Color(1, 1, 1, 0.5f), 0.25f, Interpolation.sine))));
		}
		else if (colorName.equals("blue"))
		{
			drawColor = blueColor;
			boquet_blue.setUseLightenFade(true);
			boquet_blue.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 1, 1, 0), 0.25f, Interpolation.sine), Actions.color(new Color(1, 1, 1, 0.5f), 0.25f, Interpolation.sine))));
		}
		else if (colorName.equals("pink"))
		{
			drawColor = pinkColor;
			boquet_pink.setUseLightenFade(true);
			boquet_pink.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 1, 1, 0), 0.25f, Interpolation.sine), Actions.color(new Color(1, 1, 1, 0.5f), 0.25f, Interpolation.sine))));
		}
		else if (colorName.equals("yellow"))
		{
			drawColor = yellowColor;
			boquet_yellow.setUseLightenFade(true);
			boquet_yellow.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 1, 1, 0), 0.25f, Interpolation.sine), Actions.color(new Color(1, 1, 1, 0.5f), 0.25f, Interpolation.sine))));
		}
		else
		{
			drawColor = null;
			
			boquet_red.setTouchable(Touchable.disabled);
			boquet_yellow.setTouchable(Touchable.disabled);
			boquet_pink.setTouchable(Touchable.disabled);
			boquet_green.setTouchable(Touchable.disabled);
			boquet_blue.setTouchable(Touchable.disabled);
			boquet_white.setTouchable(Touchable.disabled);
			
			boquet_color.clearActions();
			boquet_color.getColor().a = 0;
			boquet_color.setScale(0);
			boquet_color.addAction(Actions.parallel(Actions.fadeIn(0.25f, Interpolation.sine), Actions.scaleTo(1, 1, 0.25f, Interpolation.sine)));

			pen.clearActions();
			pen.getColor().a = 1;
			pen.setScale(1);
			pen.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));

			boquet_red.clearActions();
			boquet_red.getColor().a = 1;
			boquet_red.setScale(1);
			boquet_red.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));

			boquet_yellow.clearActions();
			boquet_yellow.getColor().a = 1;
			boquet_yellow.setScale(1);
			boquet_yellow.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));

			boquet_pink.clearActions();
			boquet_pink.getColor().a = 1;
			boquet_pink.setScale(1);
			boquet_pink.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));

			boquet_green.clearActions();
			boquet_green.getColor().a = 1;
			boquet_green.setScale(1);
			boquet_green.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));

			boquet_blue.clearActions();
			boquet_blue.getColor().a = 1;
			boquet_blue.setScale(1);
			boquet_blue.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));

			boquet_white.clearActions();
			boquet_white.getColor().a = 1;
			boquet_white.setScale(1);
			boquet_white.addAction(Actions.parallel(Actions.fadeOut(0.25f, Interpolation.sine), Actions.scaleTo(0, 0, 0.25f, Interpolation.sine)));			
		}
	}
	
	private void drawCircle(int x, int y) {
		if (lastModification >= 49) {
			return;
		}

		Pixmap.setBlending(Blending.SourceOver);
		
		int halpDrawX = drawMask.getWidth() / 2;
		int halpDrawY = drawMask.getHeight() / 2;
		
		int startI = (int) (x - drawMask.getWidth() / 2);
		int endI = (int) (x + drawMask.getWidth() / 2);

		int startJ = (int) (y - drawMask.getHeight() / 2);
		int endJ = (int) (y + drawMask.getHeight() / 2);

		int boquetAlpha = 0;

		for (int i = startI; i < endI; i++) {
			for (int j = startJ; j < endJ; j++) {
				boquetAlpha = drawPixmap.getPixel(i, j - halpDrawY) & 0x000000FF;

				if (boquetAlpha > 225) {
					boquetAlpha = drawMask.getPixel(i - x + halpDrawX, j - y + halpDrawY) & 0x000000FF;
					
					if (boquetAlpha > 0)
					{
						drawPixmap.drawPixel(i, j - halpDrawY, Color.rgba8888(drawColor.r, drawColor.g, drawColor.b, boquetAlpha / 255.0f));
					}
				}
			  }
			}

		Pixmap.setBlending(Blending.None);

		modifications[lastModification++].set((x), (y), drawMask.getWidth());
	}

    public int getScreenNumb() {
        return 4;
    }
    
    public MakeWrapScreen(Game game, String name, ActionResolver actionResolver) {
        super(game, name, actionResolver);

    	for (int i = 0; i < modifications.length; i++) {
    		  this.modifications[i] = new PixmapChange();
    	}

        // ----------BACKGROUND---------------
        background = new GameObject("background", false, false);
        background.setPositionXY(0, 0);
        background.folderName = "main";
        background.setOrigin(240, 754);
        background.createParticle("stars", true);
        rootGroup.addActor(background);
        // -------------------------------------------
        
        // ----------CLOUDS1---------------
        clouds1 = new GameObject("clouds", false, true);
        clouds1.setPositionXY(-10, 360);
        clouds1.setOrigin(241, 222);
        clouds1.getColor().a = 0.7f;
        clouds1.setScale(1.33f);
        clouds1.setRotation(30);
        clouds1.addAction(Actions.forever(Actions.rotateBy(-1, 0.75f)));
        clouds1.folderName = "main";
        rootGroup.addActor(clouds1);
        // -------------------------------------------
        
        // ----------SUN---------------
        GameObject sun = new GameObject("sun", false, true);
        sun.setPositionXY(41, 416);
        sun.setColor(1, 1, 0.9f, 1);
        sun.setScale(1.33f);
        sun.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 0.9f, 0.9f, 0.9f), 10, Interpolation.sine), Actions.color(new Color(1, 1, 0.9f, 1), 10, Interpolation.sine))));
        sun.folderName = "main";
        rootGroup.addActor(sun);
        // -------------------------------------------
        
        // ----------GROUND---------------
        GameObject ground = new GameObject("ground", false, true);
        ground.setPositionXY(0, 0);
        ground.folderName = "main";
        rootGroup.addActor(ground);
        // -------------------------------------------

        // ----------FRONT---------------
        GameObject front = new GameObject("front", false, true);
        front.setPositionXY(-40, -10);
        front.folderName = "main";
        rootGroup.addActor(front);
        // -------------------------------------------

        // ----------WRAP---------------
        wrapObject = new GameObject("wrapObject", false, true);
        wrapObject.skinName = "";
        wrapObject.setPositionXY(124, 225);
        wrapObject.folderName = "makeWrap";
        wrapObject.setOrigin(117, 258);
        rootGroup.addActor(wrapObject);
        // -----------------------------

        // ----------EMPTY-WRAP---------------
        wrapObjectEmpty = new GameObject("wrapObjectEmpty", false, true);
        wrapObjectEmpty.skinName = "boquet-empty";
        wrapObjectEmpty.setPositionXY(123, 225);
        wrapObjectEmpty.setOrigin(117, 258);
        wrapObjectEmpty.folderName = "makeWrap";
        rootGroup.addActor(wrapObjectEmpty);
        // -----------------------------

    	// --------------------------------------------------------------------
    	ClickListener drawClickAreaClickListener = new ClickListener() {

    	  @Override
    	  public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) 
    	  {
    		if (drawColor == null)
    		{
    			return true;
    		}
    		  
    		lastDrawXY.x = Gdx.input.getX();
    		lastDrawXY.y = Gdx.input.getY();
    		
    		FlowerMakerGame.gameStage.screenToStageCoordinates(lastDrawXY);
    		draw_click_area.stageToLocalCoordinates(lastDrawXY);
    		
    		lastDrawXY.y = (draw_click_area.getHeight() - lastDrawXY.y);
    		
    		drawCircle((int) lastDrawXY.x, (int) (lastDrawXY.y));

    		return true;
    	  }

    	  @Override
    	  public void touchDragged(InputEvent event, float x, float y, int pointer) 
    	  {
      		if (drawColor == null)
      		{
      			return;
      		}

    		drawDragVec.x = Gdx.input.getX();
    		drawDragVec.y = Gdx.input.getY();
    		FlowerMakerGame.gameStage.screenToStageCoordinates(drawDragVec);
    		draw_click_area.stageToLocalCoordinates(drawDragVec);
    		
    		newDrawXY.x = drawDragVec.x - lastDrawXY.x;
    		newDrawXY.y = (draw_click_area.getHeight() - drawDragVec.y) - lastDrawXY.y;

  		  	if (newDrawXY.len() > 5.0f) 
  		  	{
  				lastDrawXY.x = drawDragVec.x;
  				lastDrawXY.y = (draw_click_area.getHeight() - drawDragVec.y);

  				drawCircle((int) lastDrawXY.x, (int) (lastDrawXY.y));
  			}
    	  }
    	};

    	draw_click_area = new GameObject("draw_click_area", false, true);
    	draw_click_area.skinName = "";
    	draw_click_area.setPositionXY(124, 225);
    	draw_click_area.setTouchable(Touchable.enabled);
    	draw_click_area.addListener(drawClickAreaClickListener);
    	draw_click_area.setWidth(235);
    	draw_click_area.setHeight(517);
    	draw_click_area.setOrigin(draw_click_area.getWidth() / 2, draw_click_area.getHeight() / 2);
    	draw_click_area.folderName = "main";
    	rootGroup.addActor(draw_click_area);        
    	// --------------------------------------------------------------------
    	
        // ----------PALETTE---------------
        ClickListener paletteClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!pop_up_window.getOnScreen())
                {
	            	AudioPlayer.playSound("buttonSound02", 0.25f);
	
	                event.getTarget().clearActions();
	                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME / 2), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME / 2)));
	
	                pop_up_window.show();
                }
                
                return true;
            }
        };
        
        GameObject palette = new GameObject("palette", false, true);
        palette.setPositionXY(34, 78);
        palette.folderName = "makeWrap";
        palette.setTouchable(Touchable.enabled);
        palette.addListener(paletteClickListener);
        rootGroup.addActor(palette);
        // -------------------------------------------

        // ----------PEN---------------
        pen = new GameObject("pen", false, true);
        pen.setOrigin(31, 27);
        pen.setPositionXY(41, 198);
        pen.folderName = "makeWrap";
        rootGroup.addActor(pen);
        // -------------------------------------------

        // ----------BOQUET-COLOR---------------
        boquet_color = new GameObject("boquet-color", false, true);
        boquet_color.setOrigin(27, 32);
        boquet_color.setPositionXY(372, 195);
        boquet_color.folderName = "makeWrap";
        boquet_color.getColor().a = 0;
        rootGroup.addActor(boquet_color);
        // -------------------------------------------

        // ----------BOQUET-COLOR-RED---------------
        ClickListener redClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound02", 0.25f);
            	selectColor("red");
                return true;
            }
        };
        
        boquet_red = new GameObject("color-red", false, true);
        boquet_red.setOrigin(20, 20);
        boquet_red.setPositionXY(110, 188);
        boquet_red.folderName = "makeWrap";
        boquet_red.addListener(redClickListener);
        rootGroup.addActor(boquet_red);
        // -------------------------------------------

        // ----------BOQUET-COLOR-YELLOW---------------
        ClickListener yelowClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound02", 0.25f);
            	selectColor("yellow");
                return true;
            }
        };
        
        boquet_yellow = new GameObject("color-yellow", false, true);
        boquet_yellow.setOrigin(20, 20);
        boquet_yellow.setPositionXY(148, 181);
        boquet_yellow.addListener(yelowClickListener);
        boquet_yellow.folderName = "makeWrap";
        rootGroup.addActor(boquet_yellow);
        // -------------------------------------------

        // ----------BOQUET-COLOR-PINK---------------
        ClickListener pinkClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound02", 0.25f);
            	selectColor("pink");
                return true;
            }
        };

        boquet_pink = new GameObject("color-pink", false, true);
        boquet_pink.setOrigin(20, 20);
        boquet_pink.setPositionXY(189, 176);
        boquet_pink.folderName = "makeWrap";
        boquet_pink.addListener(pinkClickListener);
        rootGroup.addActor(boquet_pink);
        // -------------------------------------------

        // ----------BOQUET-COLOR-GREEN---------------
        ClickListener greenClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound02", 0.25f);
            	selectColor("green");
                return true;
            }
        };

        boquet_green = new GameObject("color-green", false, true);
        boquet_green.setOrigin(20, 20);
        boquet_green.setPositionXY(233, 175);
        boquet_green.addListener(greenClickListener);
        boquet_green.folderName = "makeWrap";
        rootGroup.addActor(boquet_green);
        // -------------------------------------------

        // ----------BOQUET-COLOR-BLUE---------------
        ClickListener blueClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound02", 0.25f);
            	selectColor("blue");
                return true;
            }
        };

        boquet_blue = new GameObject("color-blue", false, true);
        boquet_blue.setOrigin(20, 20);
        boquet_blue.setPositionXY(274, 178);
        boquet_blue.folderName = "makeWrap";
        boquet_blue.addListener(blueClickListener);
        rootGroup.addActor(boquet_blue);
        // -------------------------------------------

        // ----------BOQUET-COLOR-WHITE---------------
        ClickListener whiteClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	AudioPlayer.playSound("buttonSound02", 0.25f);
            	selectColor("white");
                return true;
            }
        };

        boquet_white = new GameObject("color-white", false, true);
        boquet_white.setOrigin(20, 20);
        boquet_white.setPositionXY(318, 184);
        boquet_white.folderName = "makeWrap";
        boquet_white.addListener(whiteClickListener);
        rootGroup.addActor(boquet_white);
        // -------------------------------------------
        
        // ----------ZOOM-IN-BTN---------------
        ClickListener zoomInClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	isZoomInPressed = true;
            	
                AudioPlayer.playSound("buttonSound02", 0.25f);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            	
                return true;
            }
            
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) 
            {
            	super.touchUp(event, x, y, pointer, button);
            	
                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            	
            	isZoomInPressed = false;
            }
        };
        
        GameObject zoom_in_btn = new GameObject("plus-btn", false, true);
        zoom_in_btn.setPositionXY(65, 265);
        zoom_in_btn.folderName = "makeWrap";
        zoom_in_btn.setTouchable(Touchable.enabled);
        zoom_in_btn.addListener(zoomInClickListener);
        rootGroup.addActor(zoom_in_btn);
        // -------------------------------------------

        // ----------ZOOM-OUT-BTN---------------
        ClickListener zoomOutClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	isZoomOutPressed = true;
            	
                AudioPlayer.playSound("buttonSound02", 0.25f);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            	
                return true;
            }
            
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) 
            {
            	super.touchUp(event, x, y, pointer, button);
            	
                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            	
            	isZoomOutPressed = false;
            }
        };
        
        GameObject zoom_out_btn = new GameObject("minus-btn", false, true);
        zoom_out_btn.setPositionXY(307, 268);
        zoom_out_btn.folderName = "makeWrap";
        zoom_out_btn.setTouchable(Touchable.enabled);
        zoom_out_btn.addListener(zoomOutClickListener);
        rootGroup.addActor(zoom_out_btn);
        // -------------------------------------------

        // ----------CLEAR-BTN---------------
        ClickListener clearClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

    			selectColor("red");					

    	    	try {
    	    		drawPixmap.dispose();
    	    	} catch (final Exception ex) {
    	    	}
    	    	drawPixmap = new Pixmap (Gdx.files.internal("data/atlas/makeWrap/boquet-1.png"));

    		  	try {
    		  		drawTexture.dispose();
    		  	} catch (final Exception ex) {
    		  	}
    		  	drawTexture = new Texture(drawPixmap);
    		  	drawTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
    		  	
    		  	if (drawColor != null)
    		  	{
    				wrapObject.skinRegion = new TextureRegion(drawTexture);
    				wrapObject.setWidth(wrapObject.skinRegion.getRegionWidth());
    				wrapObject.setHeight(wrapObject.skinRegion.getRegionHeight());
    				wrapObject.setScale(1);
    		  	}
    		  	
    			BlockedGameObject bGO = pop_up_window.getElement("boquet-1");
    			bGO.skinName = "";
    			bGO.skinRegion = new TextureRegion(drawTexture);
    			bGO.setWidth(bGO.skinRegion.getRegionWidth());
    			bGO.setHeight(bGO.skinRegion.getRegionHeight());    		  	

                return true;
            }
        };

        clear_btn = new GameObject("close-btn", false, true);
        clear_btn.setPositionXY(10, 754);
        clear_btn.folderName = "main";
        clear_btn.setTouchable(Touchable.enabled);
        clear_btn.addListener(clearClickListener);
        rootGroup.addActor(clear_btn);
        // -------------------------------------------
        
        // ----------HOME-BTN---------------
        ClickListener homeClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("mainMenu");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        home_btn = new GameObject("home-btn", false, true);
        home_btn.setPositionXY(355, 780);
        home_btn.folderName = "main";
        home_btn.setTouchable(Touchable.enabled);
        home_btn.addListener(homeClickListener);
        rootGroup.addActor(home_btn);
        // -------------------------------------------
        
        // ----------NEXT-BTN---------------
        ClickListener nextClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                
            	fbo.begin();
            	Gdx.graphics.getGL20().glClearColor(0f, 0f, 0f, 0f);
            	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                spriteBatch.begin();
                GameObject.setSavingState(true);
                wrapObject.draw(spriteBatch, 1.0f);
                wrapObjectEmpty.draw(spriteBatch, 1.0f);
                GameObject.setSavingState(false);
                spriteBatch.end();
                
            	Gdx.gl.glPixelStorei(GL20.GL_PACK_ALIGNMENT, 1);
            	Gdx.gl.glReadPixels(0, 0, FlowerMakerGame.VIRTUAL_GAME_WIDTH, FlowerMakerGame.VIRTUAL_GAME_HEIGHT, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, pixmapBuffer.getPixels());
            	fbo.end();
                
                FlowerMakerGame.mScreenChangeHelper.changeScene("makeBouquet");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        GameObject next_btn = new GameObject("next-btn", false, true);
        next_btn.setPositionXY(355, 16);
        next_btn.setOrigin(55, 39);
        next_btn.folderName = "main";
        next_btn.setTouchable(Touchable.enabled);
        next_btn.addListener(nextClickListener);
        rootGroup.addActor(next_btn);
        // -------------------------------------------

        // ----------BACK-BTN---------------
        ClickListener backClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene(getPrevScreenName());

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        GameObject back_btn = new GameObject("back-btn", false, true);
        back_btn.setPositionXY(15, 16);
        back_btn.folderName = "main";
        back_btn.setTouchable(Touchable.enabled);
        back_btn.addListener(backClickListener);
        rootGroup.addActor(back_btn);
        // -------------------------------------------
        
        //navBarPanel = new NavigationBarSimple(9, 700, "back", "arrow-left", "arrow-right", "navBar", this, 5, 40, 18);
        
        // ----------POP-UP-WINDOW---------------
        pop_up_window = new PopUpWindowSimple("pocket-back", "boquet-left", "boquet-right", "makeWrap", this, 2, 2, 75, 15, true, true);
        
        for (int i = 1; i <= 22; i++)
        {
        	if (i <= 7)
        	{
        		pop_up_window.addElement("boquet-" + String.valueOf(i), FlowerBlockType.FREE, 1.0f);
        	}
        	else
        	{
        		pop_up_window.addElement("boquet-" + String.valueOf(i), FlowerBlockType.IS_DECORATIONS, 1.0f);
        	}
        }
        
        /*pop_up_window = new PopUpWindowSimple("pocket-back", "boquet-left", "boquet-right", "navBar", this, 3, 3, 75, 15, true);
        
        for (int i = 1; i <= 24; i++)
        {
        	pop_up_window.addElement("subcat-" + String.valueOf(i), false, 1.0f);
        }*/      
        // -------------------------------------------
        
        FlowerMakerGame.gameStage.addActor(rootGroup);
    }

    private void tryUpdateSubTexture() 
    {
    	if (lastModification == 0) {
    	  return;
    	}

    	Gdx.gl.glBindTexture(GL20.GL_TEXTURE_2D, this.drawTexture.getTextureObjectHandle());

    	int width = drawPixmap.getWidth();
    	int height = drawPixmap.getHeight();

    	if (lastModification != 0) {
    	  for (int i = 0; i < lastModification; i++) {

    		PixmapChange pixmapChange = modifications[i];

    		int dstWidth = renderPixmap64.getWidth();
    		int dstHeight = renderPixmap64.getHeight();

    		int x = Math.round(pixmapChange.x) - dstWidth / 2;
    		int y = Math.round(pixmapChange.y) - dstHeight / 2;

    		if (x + dstWidth > width) {
    		  x = width - dstWidth;
    		} else if (x < 0) {
    		  x = 0;
    		}

    		if (y + dstHeight > height) {
    		  y = height - dstHeight;
    		} else if (y < 0) {
    		  y = 0;
    		}

    		renderPixmap64.drawPixmap(drawPixmap, 0, 0, x, y, dstWidth, dstHeight);

    		Gdx.gl.glTexSubImage2D(GL20.GL_TEXTURE_2D, 0, x, y, dstWidth, dstHeight, GL20.GL_RGBA, renderPixmap64.getGLType(), renderPixmap64.getPixels());
    	  }

    	  lastModification = 0;
    	}
    }
    
    @Override
    public void render(float delta) {
        clipBounds.set(rootGroup.getX(), rootGroup.getY(), CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT);

        ScissorStack.calculateScissors(camera, CasualWorkshopGame.START_X, CasualWorkshopGame.START_Y, CasualWorkshopGame.SCREEN_WIDTH, CasualWorkshopGame.SCREEN_HEIGHT, spriteBatch.getTransformMatrix(), clipBounds, scissors);
        boolean isPushed = ScissorStack.pushScissors(scissors);

        spriteBatch.begin();
        rootGroup.draw(spriteBatch, 1);
        spriteBatch.end();

        if(isPushed){
            ScissorStack.popScissors();
        }
        
    	tryUpdateSubTexture();

		if (isZoomInPressed && wrapObject.getScaleX() < 1.25f)
		{
			wrapObject.setScale(wrapObject.getScaleX() + 0.05f * delta);
		}
		else if (isZoomOutPressed && wrapObject.getScaleX() > 0.75f)
		{
			wrapObject.setScale(wrapObject.getScaleX() - 0.05f * delta);
		}
		
        rootGroup.act(delta);
        
    	draw_click_area.setScale(wrapObject.getScaleX());
    	wrapObjectEmpty.setScale(wrapObject.getScaleX());

        if (!firstEnterFrame && Gdx.input.isKeyPressed(Input.Keys.BACK) && rootGroup.getActions().size == 0) {
            FlowerMakerGame.mScreenChangeHelper.changeScene(getPrevScreenName(), false);
        }

        if (!firstEnterFrame && Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) && rootGroup.getActions().size == 0) {
            FlowerMakerGame.mScreenChangeHelper.changeScene(getPrevScreenName(), false);
        }

        if (firstEnterFrame && !Gdx.input.isKeyPressed(Input.Keys.BACK) && !Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {
            firstEnterFrame = false;
        }
    }

    @Override
    protected void resetVariables(){
    	pop_up_window.resetState();
    }

    @Override
    protected String getPrevScreenName() {
        return "protectMg";
    }

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

    @Override
    public void show() {
        /*for (int i = 1; i <= 12; i++)
        {
        	navBarPanel.addElement("category-" + String.valueOf(i), false);
        }        
        navBarPanel.resetState();*/
    	
    	Pixmap.setBlending(Blending.None);
    	pixmapBuffer.setColor(1, 1, 1, 0);
    	pixmapBuffer.fill();
    	
    	try {
    		drawPixmap.dispose();
    	} catch (final Exception ex) {
    	}
    	drawPixmap = new Pixmap (Gdx.files.internal("data/atlas/makeWrap/boquet-1.png"));

    	try {
    		  drawTexture.dispose();
      	} catch (final Exception ex) {
      	}
    	drawTexture = new Texture(drawPixmap);
    	drawTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

    	selectColor("red");

		wrapObject.skinRegion = new TextureRegion(this.drawTexture);
		wrapObject.setWidth(wrapObject.skinRegion.getRegionWidth());
		wrapObject.setHeight(wrapObject.skinRegion.getRegionHeight());
		
		BlockedGameObject bGO = pop_up_window.getElement("boquet-1");
		bGO.skinName = "";
		bGO.skinRegion = new TextureRegion(this.drawTexture);
		bGO.setWidth(bGO.skinRegion.getRegionWidth());
		bGO.setHeight(bGO.skinRegion.getRegionHeight());
		
		isZoomInPressed = false;
		isZoomOutPressed = false;
		
		super.show();
    }	

    @Override
    public void hide() {
    	super.hide();
    }
    
	@Override
	public void onPreLoad() {
		renderPixmap64 = new Pixmap(64, 64, Format.RGBA8888);
		drawMask = new Pixmap(Gdx.files.internal("data/atlas/makeWrap/draw-mask.png"));
		
		if (pixmapBuffer == null)
		{
			pixmapBuffer = new Pixmap(FlowerMakerGame.VIRTUAL_GAME_WIDTH, FlowerMakerGame.VIRTUAL_GAME_HEIGHT, Format.RGBA8888);
		}
		
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				fbo = new FrameBuffer(Pixmap.Format.RGBA8888, CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT, false);
			}
		});
	}

	@Override
	public void onPreUnLoad() {
		try {
			renderPixmap64.dispose();
		} catch (final Exception ex) {
		}
		
		try {
			drawMask.dispose();
		} catch (final Exception ex) {
		}

    	try {
    		drawPixmap.dispose();
    	} catch (final Exception ex) {
    	}
    	
		Gdx.app.postRunnable(new Runnable() {
			
			@Override
			public void run() {
				try {
					  fbo.dispose();
					  fbo = null;
					} catch (final Exception ex) {
				}
			}
		});
		
		Gdx.app.postRunnable(new Runnable() {
			
			@Override
			public void run() {
		    	try {
		    		  drawTexture.dispose();
		      	} catch (final Exception ex) {
		      	}
			}
		});	
	}
	
    @Override
    public void resume() {
	  	try {
	  		drawTexture.dispose();
	  	} catch (final Exception ex) {
	  	}

	  	drawTexture = new Texture(drawPixmap);
	  	drawTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	  	
	  	if (drawColor != null)
	  	{
			wrapObject.skinRegion = new TextureRegion(this.drawTexture);
			wrapObject.setWidth(wrapObject.skinRegion.getRegionWidth());
			wrapObject.setHeight(wrapObject.skinRegion.getRegionHeight());
	  	}
	  	
		BlockedGameObject bGO = pop_up_window.getElement("boquet-1");
		bGO.skinName = "";
		bGO.skinRegion = new TextureRegion(this.drawTexture);
		bGO.setWidth(bGO.skinRegion.getRegionWidth());
		bGO.setHeight(bGO.skinRegion.getRegionHeight());
    }
    
	@Override
	public void dispose() {
		try {
			pixmapBuffer.dispose();
		} catch (final Exception ex) {
		}
	}

	@Override
	public void onScenePlaced() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onElementSelected(TextureRegion skinRegion, int index) {
		if (index > 1)
		{
			wrapObject.skinRegion = skinRegion;
			wrapObject.setWidth(skinRegion.getRegionWidth());
			wrapObject.setHeight(skinRegion.getRegionHeight());
		}

		if (index == 1 && pen.getColor().a < 1.0f)
		{
			wrapObject.skinRegion = new TextureRegion(this.drawTexture);
			wrapObject.setWidth(skinRegion.getRegionWidth());
			wrapObject.setHeight(skinRegion.getRegionHeight());

			selectColor("red");					
		}
		else if (index != 1 && boquet_color.getColor().a < 1.0f)
		{
			selectColor("");					
		}
	}

	@Override
	public void onCategotySelected(GameObject selectedCategory, int index) {
		if (/*index == 1 && */!pop_up_window.getOnScreen())
		{
        	AudioPlayer.playSound("buttonSound02", 0.25f);

        	selectedCategory.clearActions();
        	selectedCategory.addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME / 2), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME / 2)));

            pop_up_window.show();
		}
	}

    @Override
    protected void onAdsLoad(int adsHeight){
        home_btn.setPositionY(780 - adsHeight);
        clear_btn.setPositionY(754 - adsHeight);
    }
}
