package com.ronygames.flowermaker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.casualWorkshop.CasualWorkshopGame;
import com.casualWorkshop.callbacks.ActionResolver;
import com.casualWorkshop.callbacks.IClickCallback;
import com.casualWorkshop.callbacks.PopUpElemSelectedCallback;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.objects.GameObject;
import com.casualWorkshop.objects.PopUpWindowSimple;
import com.casualWorkshop.screens.AbstractScreen;
import com.ronygames.flowermaker.FlowerMakerGame;
import com.ronygames.flowermaker.enums.FlowerBlockType;

public class MakeBouquetScreen extends AbstractScreen implements PopUpElemSelectedCallback {

    private GameObject home_btn;
    private GameObject add_flowers_btn; 
    private GameObject next_btn;
    private GameObject back_btn;
    private GameObject scale_in_btn; 
    private GameObject scale_out_btn; 
    private GameObject rotate_btn;
    
	private PopUpWindowSimple pop_up_window;
	
	private Group rootGroupFlowers;
	
	private GameObject wrapObject;
	
	private boolean isScaleIn = false;
	private boolean isScaleOut = false;
	private boolean isRotate = false;
    private boolean isRotateInv = false;

	private GameObject activeGameObject;
	private Texture drawTexture;
	
	private Pixmap pixmapBuffer;
	private FrameBuffer fbo;

	public Pixmap getBouquetPixmap()
	{
		return pixmapBuffer;
	}
	
    public int getScreenNumb() {
        return 5;
    }

    private void setActiveElement(GameObject a)
    {
    	if (a != null && a.equals(activeGameObject))
    	{
    		return;
    	}

    	if (activeGameObject != null)
    	{
	    	activeGameObject.clearActions();
	    	activeGameObject.setColor(1.0f, 1.0f, 1.0f, 1.0f);
	    	activeGameObject.setUseLightenFade(false);
	    	activeGameObject = null;
    	}
    	
    	if (a != null)
    	{
    		AudioPlayer.playSound("buttonSound02", 0.25f);
	    	activeGameObject = a;
	    	activeGameObject.setUseLightenFade(true);
	    	activeGameObject.setColor(1.0f, 1.0f, 1.0f, 0.1f);
	    	activeGameObject.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1.0f, 1.0f, 1.0f, 0.25f), Constants.BUTTONS_PRESS_TIME * 2.0f), Actions.color(new Color(0.75f, 0.75f, 0.75f, 0.1f), Constants.BUTTONS_PRESS_TIME * 2.0f))));
	    	rootGroupFlowers.addActor(activeGameObject);
    	}
    }
    
    public MakeBouquetScreen(Game game, String name, ActionResolver actionResolver) {
        super(game, name, actionResolver);
        
        // ----------BACKGROUND---------------
        ClickListener backgroundClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

            	setActiveElement(null);
                return true;
            }
        };
        
        GameObject background = new GameObject("background", false, false);
        background.setPositionXY(0, 0);
        background.folderName = "main";
        background.setOrigin(240, 754);
        background.createParticle("stars", true);
        background.setTouchable(Touchable.enabled);
        background.addListener(backgroundClickListener);
        rootGroup.addActor(background);
        // -------------------------------------------
        
        // ----------CLOUDS1---------------
        GameObject clouds1 = new GameObject("clouds", false, true);
        clouds1.setPositionXY(-10, 360);
        clouds1.setOrigin(241, 222);
        clouds1.getColor().a = 0.7f;
        clouds1.setScale(1.33f);
        clouds1.setRotation(30);
        clouds1.addAction(Actions.forever(Actions.rotateBy(-1, 0.75f)));
        clouds1.folderName = "main";
        rootGroup.addActor(clouds1);
        // -------------------------------------------
        
        // ----------SUN---------------
        GameObject sun = new GameObject("sun", false, true);
        sun.setPositionXY(41, 416);
        sun.setColor(1, 1, 0.9f, 1);
        sun.setScale(1.33f);
        sun.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 0.9f, 0.9f, 0.9f), 10, Interpolation.sine), Actions.color(new Color(1, 1, 0.9f, 1), 10, Interpolation.sine))));
        sun.folderName = "main";
        rootGroup.addActor(sun);
        // -------------------------------------------
        
        // ----------GROUND---------------
        GameObject ground = new GameObject("ground", false, true);
        ground.setPositionXY(0, 0);
        ground.folderName = "main";
        rootGroup.addActor(ground);
        // -------------------------------------------

        // ----------FRONT---------------
        GameObject front = new GameObject("front", false, true);
        front.setPositionXY(-40, -10);
        front.folderName = "main";
        rootGroup.addActor(front);
        // -------------------------------------------

        // ----------ADD-FLOWERS-BTN---------------
        ClickListener addFlowersClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!pop_up_window.getOnScreen())
                {
	            	AudioPlayer.playSound("buttonSound02", 0.25f);
	
	                event.getTarget().clearActions();
	                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME / 2), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME / 2)));
	
	                pop_up_window.show();
                }

                return true;
            }
        };
        
        add_flowers_btn = new GameObject("add-btn", false, true);
        add_flowers_btn.setPositionXY(148, 16);
        add_flowers_btn.folderName = "makeBouquet";
        add_flowers_btn.setTouchable(Touchable.enabled);
        add_flowers_btn.addListener(addFlowersClickListener);
        rootGroup.addActor(add_flowers_btn);
        // -------------------------------------------
        
        // ----------HOME-BTN---------------
        ClickListener homeClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("mainMenu");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        home_btn = new GameObject("home-btn", false, true);
        home_btn.setPositionXY(355, 780);
        home_btn.folderName = "main";
        home_btn.setTouchable(Touchable.enabled);
        home_btn.addListener(homeClickListener);
        rootGroup.addActor(home_btn);
        // -------------------------------------------
        
        // ----------NEXT-BTN---------------
        ClickListener nextClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                
            	/*add_flowers_btn.setVisible(false);
            	home_btn.setVisible(false); 
            	next_btn.setVisible(false);
            	back_btn.setVisible(false);
            	scale_in_btn.setVisible(false);
            	scale_out_btn.setVisible(false);
            	rotate_btn.setVisible(false);*/
            	setActiveElement(null);
            	
            	fbo.begin();
            	Gdx.graphics.getGL20().glClearColor(0f, 0f, 0f, 0f);
            	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

                spriteBatch.begin();
                GameObject.setSavingState(true);
                //rootGroup.draw(spriteBatch, 1);
                wrapObject.draw(spriteBatch, 1);
                rootGroupFlowers.draw(spriteBatch, 1);
                GameObject.setSavingState(false);
                spriteBatch.end();
                
            	Gdx.gl.glPixelStorei(GL20.GL_PACK_ALIGNMENT, 1);
            	Gdx.gl.glReadPixels(0, 0, FlowerMakerGame.VIRTUAL_GAME_WIDTH, FlowerMakerGame.VIRTUAL_GAME_HEIGHT, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, pixmapBuffer.getPixels());
            	fbo.end();
                
            	/*add_flowers_btn.setVisible(true);
            	home_btn.setVisible(true); 
            	next_btn.setVisible(true);
            	back_btn.setVisible(true);
            	scale_in_btn.setVisible(true);
            	scale_out_btn.setVisible(true);
            	rotate_btn.setVisible(true);*/                
                
                FlowerMakerGame.mScreenChangeHelper.changeScene("final");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        next_btn = new GameObject("next-btn", false, true);
        next_btn.setPositionXY(355, 16);
        next_btn.setOrigin(55, 39);
        next_btn.folderName = "main";
        next_btn.setTouchable(Touchable.enabled);
        next_btn.addListener(nextClickListener);
        rootGroup.addActor(next_btn);
        // -------------------------------------------

        // ----------BACK-BTN---------------
        ClickListener backClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene(getPrevScreenName());

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        back_btn = new GameObject("back-btn", false, true);
        back_btn.setPositionXY(15, 16);
        back_btn.folderName = "main";
        back_btn.setTouchable(Touchable.enabled);
        back_btn.addListener(backClickListener);
        rootGroup.addActor(back_btn);
        // -------------------------------------------

        // ----------WRAP---------------
        wrapObject = new GameObject("wrapObject", false, true);
        wrapObject.skinName = "";
        wrapObject.setPositionXY(0, -70); //TODO: remove this hack
        wrapObject.folderName = "main";
        wrapObject.setScaleY(-1);
        wrapObject.setOrigin(FlowerMakerGame.VIRTUAL_GAME_WIDTH / 2, FlowerMakerGame.VIRTUAL_GAME_HEIGHT / 2);
        rootGroup.addActor(wrapObject);
        // -----------------------------

    	rootGroupFlowers = new Group();
    	rootGroupFlowers.setName("rootGroupFlowers");
    	rootGroupFlowers.setTouchable(Touchable.childrenOnly);
    	rootGroupFlowers.setWidth(FlowerMakerGame.VIRTUAL_GAME_WIDTH);
    	rootGroupFlowers.setHeight(FlowerMakerGame.VIRTUAL_GAME_HEIGHT);
    	rootGroup.addActor(rootGroupFlowers);

        // ----------SCALE-IN-BTN---------------
        ClickListener scaleInClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                isScaleIn = true;
                
                return true;
            }
            
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) 
            {
            	super.touchUp(event, x, y, pointer, button);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

            	isScaleIn = false;
            }
        };
        
        scale_in_btn = new GameObject("zoom-in-btn", false, true);
        scale_in_btn.setPositionXY(20, 270);
        scale_in_btn.folderName = "makeBouquet";
        scale_in_btn.setTouchable(Touchable.enabled);
        scale_in_btn.addListener(scaleInClickListener);
        rootGroup.addActor(scale_in_btn);
        // -------------------------------------------
        
        // ----------SCALE-OUT-BTN---------------
        ClickListener scaleOutClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                isScaleOut = true;
                
                return true;
            }
            
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) 
            {
            	super.touchUp(event, x, y, pointer, button);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                isScaleOut = false;
            }
        };
        
        scale_out_btn = new GameObject("zoom-out-btn", false, true);
        scale_out_btn.setPositionXY(20, 200);
        scale_out_btn.folderName = "makeBouquet";
        scale_out_btn.setTouchable(Touchable.enabled);
        scale_out_btn.addListener(scaleOutClickListener);
        rootGroup.addActor(scale_out_btn);
        // -------------------------------------------
        
        // ----------ROTATE-BTN---------------
        ClickListener rotateOutClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                isRotate = true;
                
                return true;
            }
            
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) 
            {
            	super.touchUp(event, x, y, pointer, button);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                isRotate = false;
            }
        };
        
        rotate_btn = new GameObject("rotate-btn", false, true);
        rotate_btn.setPositionXY(400, 270);
        rotate_btn.folderName = "makeBouquet";
        rotate_btn.setTouchable(Touchable.enabled);
        rotate_btn.addListener(rotateOutClickListener);
        rootGroup.addActor(rotate_btn);
        // -------------------------------------------


        // ----------ROTATE-BTN---------------
        ClickListener rotateInClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                isRotateInv = true;

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                super.touchUp(event, x, y, pointer, button);

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                isRotateInv = false;
            }
        };

        GameObject rotateInv = new GameObject("rotate-btn", false, true);
        rotateInv.setPositionXY(400, 200);
        rotateInv.folderName = "makeBouquet";
        rotateInv.setTouchable(Touchable.enabled);
        rotateInv.setOrigin(36, 34);
        rotateInv.setScaleX(-1);
        rotateInv.addListener(rotateInClickListener);
        rootGroup.addActor(rotateInv);
        // -------------------------------------------

        // ----------DELETE-BTN---------------
        GameObject deleteBtn = new GameObject("close-btn", false, true);
        deleteBtn.setPositionXY(200, 120);
        deleteBtn.folderName = "main";
        deleteBtn.setClickable(true);
        deleteBtn.addClickListener(new IClickCallback() {
            @Override
            public void onClick(Actor currentActor) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                if(activeGameObject != null) {

                    activeGameObject.clearActions();
                    rootGroupFlowers.removeActor(activeGameObject);
                    activeGameObject = null;
                }

                currentActor.clearActions();
                currentActor.addAction(Actions.sequence(
                        Actions.color(new Color(0.6f, 0.6f, 0.6f, 1f), Constants.BUTTONS_PRESS_TIME * 0.5f),
                        Actions.color(new Color(1.0f, 1.0f, 1.0f, 1f), Constants.BUTTONS_PRESS_TIME * 0.5f)));
            }
        });
        rootGroup.addActor(deleteBtn);
        // -------------------------------------------
        
        // ----------POP-UP-WINDOW---------------
        pop_up_window = new PopUpWindowSimple("pocket-back", "boquet-left", "boquet-right", "makeBouquet", this, 3, 2, 95, 20, true, true);
        
        for (int i = 0; i <= 38; i++)
        {
        	if (i <= 12)
        	{
        		pop_up_window.addElement("flower" + String.valueOf(i), FlowerBlockType.FREE, 1.0f);
        	}
        	else
        	{
        		pop_up_window.addElement("flower" + String.valueOf(i), FlowerBlockType.IS_FLOWER, 1.0f);
        	}
        }
        // -------------------------------------------

        FlowerMakerGame.gameStage.addActor(rootGroup);
    }

    @Override
    public void render(float delta) {
    	super.render(delta);
    	
    	if (activeGameObject != null)
    	{
			if (isScaleIn && activeGameObject.getScaleX() < 1.5f)
			{
				activeGameObject.setScale(activeGameObject.getScaleX() + 0.15f * delta);
			}
			else if (isScaleOut && activeGameObject.getScaleX() > 0.5f)
			{
				activeGameObject.setScale(activeGameObject.getScaleX() - 0.15f * delta);
			}
			else if (isRotate)
			{
				activeGameObject.setRotation(activeGameObject.getRotation() - 50.0f * delta);
			} else if (isRotateInv)
            {
                activeGameObject.setRotation(activeGameObject.getRotation() + 50.0f * delta);
            }
    	}
    }
    
    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
    	super.hide();
    	
    	while (rootGroupFlowers.getChildren().size > 0) {
  		  Actor a = rootGroupFlowers.getChildren().get(0);
  		  a.remove();
  		  a = null;
  		}
    }
	
    @Override
    public void show() {
        super.show();

    	Pixmap.setBlending(Blending.None);
    	pixmapBuffer.setColor(1, 1, 1, 0);
    	pixmapBuffer.fill();

	  	try {
	  		drawTexture.dispose();
	  	} catch (final Exception ex) {
	  	}

	  	MakeWrapScreen prevScreen = (MakeWrapScreen) this.game.findScreen("makeWrap");
	  	drawTexture = new Texture(prevScreen.getWrapPixmap());
	  	drawTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        wrapObject.skinRegion = new TextureRegion(drawTexture);
		wrapObject.setWidth(wrapObject.skinRegion.getRegionWidth());
		wrapObject.setHeight(wrapObject.skinRegion.getRegionHeight());
		
		isScaleIn = false;
		isScaleOut = false;
		isRotate = false;
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
    }

    @Override
    public void resume() {
    	super.resume();
    	
	  	try {
	  		drawTexture.dispose();
	  	} catch (final Exception ex) {
	  	}
	  	
	  	MakeWrapScreen prevScreen = (MakeWrapScreen) this.game.findScreen("makeWrap");
	  	drawTexture = new Texture(prevScreen.getWrapPixmap());
	  	drawTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	  	
        wrapObject.skinRegion = new TextureRegion(drawTexture);
		wrapObject.setWidth(wrapObject.skinRegion.getRegionWidth());
		wrapObject.setHeight(wrapObject.skinRegion.getRegionHeight());
    }

    @Override
    public void dispose() {
		try {
			pixmapBuffer.dispose();
		} catch (final Exception ex) {
		}
    }

    @Override
    public void onPreLoad() {
		if (pixmapBuffer == null)
		{
			pixmapBuffer = new Pixmap(FlowerMakerGame.VIRTUAL_GAME_WIDTH, FlowerMakerGame.VIRTUAL_GAME_HEIGHT, Format.RGBA8888);
		}
		
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				fbo = new FrameBuffer(Pixmap.Format.RGBA8888, CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT, false);
			}
		});
    }

    @Override
    public void onPreUnLoad() {
		Gdx.app.postRunnable(new Runnable() {
			
			@Override
			public void run() {
		    	try {
		    		  drawTexture.dispose();
		      	} catch (final Exception ex) {
		      	}
			}
		});	    	
		
		Gdx.app.postRunnable(new Runnable() {
			
			@Override
			public void run() {
				try {
					  fbo.dispose();
					  fbo = null;
					} catch (final Exception ex) {
				}
			}
		});
    }

    @Override
    public void onScenePlaced() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void resetVariables() 
    {
    	pop_up_window.resetState();
    }

    @Override
    protected String getPrevScreenName() {
        return "makeWrap";
    }

	@Override
	public void onElementSelected(TextureRegion skinRegion, int index) {
        ClickListener flowerAddedClickListener = new ClickListener() {
        	private Vector2 startDrag = new Vector2();
        	private Vector2 draggingVec = new Vector2();
        	
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            	setActiveElement((GameObject) event.getTarget());
            	
            	startDrag.x = Gdx.input.getX();
            	startDrag.y = Gdx.input.getY();
            	FlowerMakerGame.gameStage.screenToStageCoordinates(startDrag);
            	startDrag.x = startDrag.x - activeGameObject.getX();
            	startDrag.y = startDrag.y - activeGameObject.getY();
            	
                return true;
            }
            
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) 
            {
            	super.touchDragged(event, x, y, pointer);
            	draggingVec.x = Gdx.input.getX();
            	draggingVec.y = Gdx.input.getY();
            	FlowerMakerGame.gameStage.screenToStageCoordinates(draggingVec);
            	
            	activeGameObject.setPositionXY(draggingVec.x - startDrag.x, draggingVec.y - startDrag.y);
            }
        };
        
		GameObject gO = new GameObject();
		gO.setName("tmpFlowerObject" + String.valueOf(rootGroupFlowers.getChildren().size));
		gO.skinName = "flower";
		gO.skinRegion = skinRegion;
		gO.setWidth(gO.skinRegion.getRegionWidth());
		gO.setHeight(gO.skinRegion.getRegionHeight());
		gO.setOrigin(gO.getWidth() / 2, gO.getHeight() / 2);
		gO.setTouchable(Touchable.enabled);
		gO.setUseSmartHit(true);
		gO.setPosition(FlowerMakerGame.VIRTUAL_GAME_WIDTH / 2 - gO.getWidth() / 2, FlowerMakerGame.VIRTUAL_GAME_HEIGHT / 2 - gO.getHeight() / 2);
		gO.addListener(flowerAddedClickListener);

		rootGroupFlowers.addActor(gO);
		setActiveElement(gO);
	}

    @Override
    protected void onAdsLoad(int adsHeight){
        home_btn.setPositionY(780 - adsHeight);
    }
}
