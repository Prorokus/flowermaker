package com.ronygames.flowermaker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.casualWorkshop.CasualWorkshopGame;
import com.casualWorkshop.callbacks.ActionResolver;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.objects.GameObject;
import com.casualWorkshop.screens.AbstractScreen;
import com.ronygames.flowermaker.callbacks.FlowersActionResolver;

public class ShopScreen extends AbstractScreen {
	
	private GameObject buy_all;
	private GameObject buy_flowers;
	private GameObject buy_decor;

    public int getScreenNumb() {
        return 0;
    }

    public ShopScreen(Game game, String name, ActionResolver actionResolver) {
        super(game, name, actionResolver);

        // ----------BACKGROUND---------------
        GameObject background = new GameObject("background", false, false);
        background.setPositionXY(0, 0);
        background.setSize(CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT);
        background.folderName = "shop";
        rootGroup.addActor(background);
        // -----------------------------------

        // ----------GROUND---------------
        GameObject ground = new GameObject("ground", false, true);
        ground.setPositionXY(0, 0);
        ground.folderName = "shop";
        rootGroup.addActor(ground);
        // -------------------------------------------
        
        // ----------CLOUDS1---------------
        GameObject clouds1 = new GameObject("clouds", false, true);
        clouds1.setPositionXY(12, 470);
        clouds1.setOrigin(241, 222);
        clouds1.getColor().a = 0.7f;
        clouds1.setRotation(30);
        clouds1.addAction(Actions.forever(Actions.rotateBy(-1, 0.75f)));
        clouds1.folderName = "main";
        rootGroup.addActor(clouds1);
        // -------------------------------------------
        
        // ----------SUN---------------
        GameObject sun = new GameObject("sun", false, true);
        sun.setPositionXY(109, 596);
        sun.setColor(1, 1, 0.9f, 1);
        sun.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 0.9f, 0.9f, 0.9f), 10, Interpolation.sine), Actions.color(new Color(1, 1, 0.9f, 1), 10, Interpolation.sine))));
        sun.folderName = "main";
        rootGroup.addActor(sun);
        // -------------------------------------------
        
        // ----------LEFT_FENCE---------------
        GameObject left_fence = new GameObject("fence2", false, true);
        left_fence.setPositionXY(-25, 116);
        left_fence.folderName = "shop";
        rootGroup.addActor(left_fence);
        // -------------------------------------------
        
        // ----------RIGHT_FENCE---------------
        GameObject right_fence = new GameObject("fence1", false, true);
        right_fence.setPositionXY(282, 73);
        right_fence.folderName = "shop";
        rootGroup.addActor(right_fence);
        // -------------------------------------------
        
        // ----------SHOP-TXT---------------
        GameObject shop_txt = new GameObject("shop-txt", false, true);
        shop_txt.setPositionXY(103, 585);
        shop_txt.setRotation(5);
        shop_txt.setOrigin(140, -20);
        shop_txt.addAction(Actions.forever(Actions.sequence(Actions.moveBy(5, 0, 1.5f, Interpolation.sine), Actions.moveBy(-5, 0, 1.5f, Interpolation.sine))));
        shop_txt.addAction(Actions.forever(Actions.sequence(Actions.rotateTo(-2, 1.5f, Interpolation.sine), Actions.rotateTo(2, 1.5f, Interpolation.sine))));
        shop_txt.folderName = "shop";
        rootGroup.addActor(shop_txt);
        // -------------------------------------------

        // ----------BUTTON_HOME----------------
        ClickListener homeClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                CasualWorkshopGame.mScreenChangeHelper.changeScene("mainMenu");

                event.getTarget().clearActions();
                event.getTarget().setColor(1, 1, 1, 1);
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };

        GameObject home_btn = new GameObject("close-btn", false, true);
        home_btn.setPositionXY(382, 767);
        home_btn.folderName = "main";
        home_btn.setTouchable(Touchable.enabled);
        home_btn.addListener(homeClickListener);
        rootGroup.addActor(home_btn);
        // -------------------------------------------
        
        // -----------------UNLOCK_ALL--------------------------
        ClickListener allClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                if (mActionResolver != null)
                {
                	((FlowersActionResolver)mActionResolver).purchaseAll(null);
                }

                event.getTarget().clearActions();
                event.getTarget().setColor(1, 1, 1, 1);
                event.getTarget().setScale(1);
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        buy_all = new GameObject("unlock-all-btn", false, true);
        buy_all.setPositionXY(113, 470);
        buy_all.setOrigin(130, 38);
        buy_all.setTouchable(Touchable.enabled);
        buy_all.addListener(allClickListener);
        buy_all.folderName = "shop";
        rootGroup.addActor(buy_all);
        // -------------------------------------------
        
        // --------------------MORE_FLOWERS-----------------------
        ClickListener flowersClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                if (mActionResolver != null)
                {
                    ((FlowersActionResolver)mActionResolver).purchaseFlowers(null);
                }

                event.getTarget().clearActions();
                event.getTarget().setColor(1, 1, 1, 1);
                event.getTarget().setScale(1);
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        buy_flowers = new GameObject("more-flowers-btn", false, true);
        buy_flowers.setPositionXY(111, 381);
        buy_flowers.setOrigin(132, 39);
        buy_flowers.setTouchable(Touchable.enabled);
        buy_flowers.addListener(flowersClickListener);
        buy_flowers.folderName = "shop";
        rootGroup.addActor(buy_flowers);
        // -------------------------------------------
        
        // --------------------MORE_DECORATIONS-----------------------
        ClickListener decorClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer,
                                     int button) {
                AudioPlayer.playSound("buttonSound02", 0.25f);

                if (mActionResolver != null)
                {
                	((FlowersActionResolver)mActionResolver).purchaseDecorations(null);
                }

                event.getTarget().clearActions();
                event.getTarget().setColor(1, 1, 1, 1);
                event.getTarget().setScale(1);
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        buy_decor = new GameObject("more-decor-btn", false, true);
        buy_decor.setPositionXY(109, 297);
        buy_decor.setOrigin(132, 35);
        buy_decor.setTouchable(Touchable.enabled);
        buy_decor.addListener(decorClickListener);
        buy_decor.folderName = "shop";
        rootGroup.addActor(buy_decor);
        // -------------------------------------------
        CasualWorkshopGame.gameStage.addActor(rootGroup);
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        firstEnterFrame = true;

        hideAds();

        resetVariables();
    }

    @Override
    public void hide() {
        rootGroup.setTouchable(Touchable.disabled);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreUnLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScenePlaced() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
            	buy_all.clearActions();
            	buy_all.setScale(1.0f);
            	buy_all.addAction(Actions.sequence(Actions.scaleTo(1.25f, 1.25f, 0.2f, Interpolation.sine), Actions.scaleTo(1.0f, 1.0f, 0.2f, Interpolation.sine)));
            	
            	buy_flowers.clearActions();
            	buy_flowers.setScale(1.0f);
            	buy_flowers.addAction(Actions.sequence(Actions.delay(0.4f), Actions.scaleTo(1.25f, 1.25f, 0.2f, Interpolation.sine), Actions.scaleTo(1.0f, 1.0f, 0.2f, Interpolation.sine)));

            	buy_decor.clearActions();
            	buy_decor.setScale(1.0f);
            	buy_decor.addAction(Actions.sequence(Actions.delay(0.8f), Actions.scaleTo(1.25f, 1.25f, 0.2f, Interpolation.sine), Actions.scaleTo(1.0f, 1.0f, 0.2f, Interpolation.sine)));
            }
        });
    }

    @Override
    protected void resetVariables() {
    }

    @Override
    protected String getPrevScreenName() {
        return "mainMenu";
    }

}
