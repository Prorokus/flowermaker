package com.ronygames.flowermaker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.casualWorkshop.CasualWorkshopGame;
import com.casualWorkshop.callbacks.ActionResolver;
import com.casualWorkshop.callbacks.IClickCallback;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.helpers.ScreenshotSaver;
import com.casualWorkshop.objects.GameObject;
import com.casualWorkshop.screens.AbstractScreen;
import com.ronygames.flowermaker.FlowerMakerGame;

import java.nio.ByteBuffer;

/**
 * Created by VLaD on 29.12.13.
 */
public class FinalScene extends AbstractScreen {

    private GameObject back_btn;
    private GameObject save;
    private GameObject share;
    private GameObject home;

    private boolean saveScene = false;
    private boolean shareScene = false;

    private boolean isSaved = false;

    private FrameBuffer fullFBO;

    private Texture drawTexture;
    private GameObject wrapObject;
    
    @Override
    public int getScreenNumb() {
        return 6;
    }
    
    public FinalScene(Game game, String name, ActionResolver actionResolver) {
        super(game, name, actionResolver);

        // ----------BACKGROUND---------------
        GameObject background = new GameObject("background", false, false);
        background.setPositionXY(0, 0);
        background.folderName = "main";
        background.setOrigin(240, 754);
        background.createParticle("stars", true);
        rootGroup.addActor(background);
        // -------------------------------------------

        // ----------CLOUDS1---------------
        GameObject clouds1 = new GameObject("clouds", false, true);
        clouds1.setPositionXY(-10, 360);
        clouds1.setOrigin(241, 222);
        clouds1.getColor().a = 0.7f;
        clouds1.setScale(1.33f);
        clouds1.setRotation(30);
        clouds1.addAction(Actions.forever(Actions.rotateBy(-1, 0.75f)));
        clouds1.folderName = "main";
        rootGroup.addActor(clouds1);
        // -------------------------------------------

        // ----------SUN---------------
        GameObject sun = new GameObject("sun", false, true);
        sun.setPositionXY(41, 416);
        sun.setColor(1, 1, 0.9f, 1);
        sun.setScale(1.33f);
        sun.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 0.9f, 0.9f, 0.9f), 10, Interpolation.sine), Actions.color(new Color(1, 1, 0.9f, 1), 10, Interpolation.sine))));
        sun.folderName = "main";
        rootGroup.addActor(sun);
        // -------------------------------------------

        // ----------GROUND---------------
        GameObject ground = new GameObject("ground", false, true);
        ground.setPositionXY(0, 0);
        ground.folderName = "main";
        rootGroup.addActor(ground);
        // -------------------------------------------

        // ----------FRONT---------------
        GameObject front = new GameObject("front", false, true);
        front.setPositionXY(-40, -10);
        front.folderName = "main";
        rootGroup.addActor(front);
        // -------------------------------------------
        
        // ----------WRAP---------------
        wrapObject = new GameObject("wrapObject", false, true);
        wrapObject.skinName = "";
        wrapObject.setPositionXY(0, 0);
        wrapObject.folderName = "main";
        wrapObject.setScaleY(-1);
        wrapObject.setOrigin(FlowerMakerGame.VIRTUAL_GAME_WIDTH / 2, FlowerMakerGame.VIRTUAL_GAME_HEIGHT / 2);
        rootGroup.addActor(wrapObject);
        // -----------------------------

        // -------------BACK_BTN------------------------------
        back_btn = new GameObject("back-btn", false, true);
        back_btn.setPositionXY(15, 16);
        back_btn.folderName = "main";
        back_btn.setClickable(true);
        back_btn.addClickListener(new IClickCallback() {
            @Override
            public void onClick(Actor currentActor) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene(getPrevScreenName());

                currentActor.clearActions();
                currentActor.addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            }
        });
        rootGroup.addActor(back_btn);
        // -------------------------------------------

        // -------------HOME_BTN------------------------------
        home = new GameObject("home", false, true);
        home.setPositionXY(385, 16);
        home.folderName = screenName;
        home.setClickable(true);
        home.addClickListener(new IClickCallback() {
            @Override
            public void onClick(Actor currentActor) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("mainMenu");

                if(mActionResolver != null){
                    mActionResolver.showInterstitial(0);
                }

                currentActor.clearActions();
                currentActor.addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            }
        });
        rootGroup.addActor(home);
        // -------------------------------------------

        // -------------SAVE_BTN------------------------------
        save = new GameObject("save", false, true);
        save.setPositionXY(15, 760);
        save.folderName = screenName;
        save.setClickable(true);
        save.addClickListener(new IClickCallback() {
            @Override
            public void onClick(Actor currentActor) {
                AudioPlayer.playSound("camera", 0.25f);

                if (mActionResolver != null) {
                    mActionResolver.showProgress();
                }

                saveScene = true;

                isSaved = true;

                currentActor.clearActions();
                currentActor.addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            }
        });
        rootGroup.addActor(save);
        // -------------------------------------------

        // -------------SHARE_BTN------------------------------
        share = new GameObject("share", false, true);
        share.setPositionXY(385, 760);
        share.folderName = screenName;
        share.setClickable(true);
        share.addClickListener(new IClickCallback() {
            @Override
            public void onClick(Actor currentActor) {
                AudioPlayer.playSound("camera", 0.25f);

                if (mActionResolver != null) {
                    mActionResolver.showProgress();
                }

                shareScene = true;

                currentActor.clearActions();
                currentActor.addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));
            }
        });
        rootGroup.addActor(share);
        // -------------------------------------------

        FlowerMakerGame.gameStage.addActor(rootGroup);
    }

    @Override
    protected String getPrevScreenName() {
        return "makeBouquet";
    }

    @Override
    public void resume() {
    	super.resume();
    	
	  	try {
	  		drawTexture.dispose();
	  	} catch (final Exception ex) {
	  	}
	  	
        MakeBouquetScreen prevScreen = (MakeBouquetScreen) this.game.findScreen("makeBouquet");
	  	drawTexture = new Texture(prevScreen.getBouquetPixmap());
	  	drawTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        wrapObject.skinRegion = new TextureRegion(drawTexture);
		wrapObject.setWidth(wrapObject.skinRegion.getRegionWidth());
		wrapObject.setHeight(wrapObject.skinRegion.getRegionHeight());
    }
    
    @Override
    public void show() {
        super.show();
        
	  	try {
	  		drawTexture.dispose();
	  	} catch (final Exception ex) {
	  	}

        MakeBouquetScreen prevScreen = (MakeBouquetScreen) this.game.findScreen("makeBouquet");
	  	drawTexture = new Texture(prevScreen.getBouquetPixmap());
	  	drawTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        wrapObject.skinRegion = new TextureRegion(drawTexture);
		wrapObject.setWidth(wrapObject.skinRegion.getRegionWidth());
		wrapObject.setHeight(wrapObject.skinRegion.getRegionHeight());
    }

    @Override
    public void render(float delta){
        if (saveScene || shareScene) {
            prepareBeforeSaving();
            fullFBO.begin();

            Gdx.graphics.getGL20().glClearColor(0f, 0f, 0f, 0f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            GameObject.setSavingState(true);
            
            spriteBatch.begin();
            rootGroup.draw(spriteBatch, 1);
            spriteBatch.end();

            fullFBO.end();
            GameObject.setSavingState(false);

            sceneSaveOrShare(shareScene);
            restoreAfterSaving(shareScene);
        } else {
            super.render(delta);
        }
    }

    private void prepareBeforeSaving(){
        back_btn.setVisible(false);
        save.setVisible(false);
        share.setVisible(false);
        home.setVisible(false);
    }

    private void restoreAfterSaving(boolean forSharing){
        if (forSharing) {
            save.setVisible(!isSaved);
        }

        share.setVisible(true);
        home.setVisible(true);
        back_btn.setVisible(true);

        saveScene = false;
        shareScene = false;
    }

    private void sceneSaveOrShare(boolean forSharing) {
        Pixmap pixmapBuffer = new Pixmap(CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT, Pixmap.Format.RGBA8888);
        pixmapBuffer.setColor(1, 1, 1, 1);
        pixmapBuffer.fill();

        fullFBO.begin();
        Gdx.gl.glPixelStorei(GL20.GL_PACK_ALIGNMENT, 1);
        ByteBuffer pixels = pixmapBuffer.getPixels();
        Gdx.gl.glReadPixels(0, 0, CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT, GL20.GL_RGBA, GL20.GL_UNSIGNED_BYTE, pixels);
        fullFBO.end();

        String savedFileName = null;
        try {
            savedFileName = ScreenshotSaver.saveScreenshot(pixmapBuffer, forSharing);

            pixels.clear();
            pixels = null;

        } catch (GdxRuntimeException e) {
        } catch (Exception e) {
        }

        if (mActionResolver != null) {

            mActionResolver.hideProgress();

            if (forSharing) {
                mActionResolver.showShareDialog(savedFileName);
            } else {
                mActionResolver.showScreenshotToast(savedFileName != null);
            }
        }
    }

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPreLoad() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPreUnLoad() {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    if(fullFBO != null){
                        fullFBO.dispose();
                        fullFBO = null;
                    }
                } catch (final Exception ex) {
                    Gdx.app.log("error","fbo dispose failed");
                }
            }
        });
        
		Gdx.app.postRunnable(new Runnable() {
			
			@Override
			public void run() {
		    	try {
		    		  drawTexture.dispose();
		      	} catch (final Exception ex) {
		      	}
			}
		});	
	}

	@Override
	public void onScenePlaced() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void resetVariables() {
        if (fullFBO == null) {
            fullFBO = new FrameBuffer(Pixmap.Format.RGBA8888, CasualWorkshopGame.VIRTUAL_GAME_WIDTH, CasualWorkshopGame.VIRTUAL_GAME_HEIGHT, false);
        }

        fullFBO.begin();
        Gdx.graphics.getGL20().glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        fullFBO.end();

        save.setVisible(true);

        isSaved = false;
	}

    @Override
    protected void onAdsLoad(int adsHeight){
        save.setPositionY(760 - adsHeight);
        share.setPositionY(760 - adsHeight);
    }
}
