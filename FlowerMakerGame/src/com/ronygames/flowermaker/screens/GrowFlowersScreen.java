package com.ronygames.flowermaker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.casualWorkshop.callbacks.ActionResolver;
import com.casualWorkshop.helpers.AudioPlayer;
import com.casualWorkshop.helpers.Constants;
import com.casualWorkshop.objects.GameObject;
import com.casualWorkshop.objects.TextGameObject;
import com.casualWorkshop.screens.AbstractScreen;
import com.ronygames.flowermaker.FlowerMakerGame;
import com.ronygames.flowermaker.helpers.Const;

import java.util.Random;

public class GrowFlowersScreen extends AbstractScreen {
	
	private GameObject back_btn;
	private GameObject next_btn;
	
	private GameObject ground_piece1;
	private GameObject ground_piece2;
	private GameObject ground_piece3;
	private GameObject showel;
	private GameObject seedsPack;
	private GameObject seeds;
	private GameObject rake;
	private GameObject water;
	private GameObject waterEffect;
	private GameObject fertilizer;
	private GameObject fertilizerEffect;
	private GameObject flower;
	
	private TextGameObject dragOverTxt;
	private TextGameObject dragOverTxt2;
	
	private Random rand = new Random();
	
    private enum GrowingState{
        DIG1,
        DIG2,
        DIG3,
        SEEDS,
        WATER,
        FERTILIZER,
        RAKE1,
        RAKE2,
        RAKE3,
        FINISHED
    }
    
    private GrowingState mGrowingState = GrowingState.DIG1;

    public int getScreenNumb() {
        return 2;
    }

    public GrowFlowersScreen(Game game, String name, ActionResolver actionResolver) {
        super(game, name, actionResolver);

        // ----------BACKGROUND---------------
        GameObject background = new GameObject("background", false, false);
        background.setPositionXY(0, 0);
        background.folderName = "main";
        background.setOrigin(240, 754);
        background.createParticle("stars", true);
        rootGroup.addActor(background);
        // -------------------------------------------

        // ----------CLOUDS1---------------
        GameObject clouds1 = new GameObject("clouds", false, true);
        clouds1.setPositionXY(5, 270);
        clouds1.setOrigin(241, 222);
        clouds1.getColor().a = 0.7f;
        clouds1.setRotation(30);
        clouds1.addAction(Actions.forever(Actions.rotateBy(-1, 0.75f)));
        clouds1.folderName = "main";
        rootGroup.addActor(clouds1);
        // -------------------------------------------
        
        // ----------SUN---------------
        GameObject sun = new GameObject("sun", false, true);
        sun.setPositionXY(97, 351);
        sun.setColor(1, 1, 0.9f, 1);
        sun.addAction(Actions.forever(Actions.sequence(Actions.color(new Color(1, 0.9f, 0.9f, 0.9f), 10, Interpolation.sine), Actions.color(new Color(1, 1, 0.9f, 1), 10, Interpolation.sine))));
        sun.folderName = "main";
        rootGroup.addActor(sun);
        // -------------------------------------------
        
        // ----------GROUND---------------
        GameObject ground = new GameObject("ground", false, true);
        ground.setPositionXY(0, 29);
        ground.folderName = "growFlowers";
        rootGroup.addActor(ground);
        // -------------------------------------------
        
        // ----------FRONT---------------
        GameObject front = new GameObject("front", false, true);
        front.setPositionXY(0, 0);
        front.folderName = "growFlowers";
        rootGroup.addActor(front);
        // -------------------------------------------
        
        // ---------------SHOWEL----------------------
        ClickListener showelClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                if (mGrowingState == GrowingState.DIG1)
                {
                	showel.clearParticles();
                	showel.setTouchable(Touchable.disabled);
                	showel.setPosition(230, 300);
                	
                	ground_piece1.addAction(Actions.sequence(Actions.delay(1.0f), Actions.moveTo(200, 200, 0.5f, Interpolation.sine)));
                	showel.addAction(Actions.sequence(Actions.moveTo(230, 200, 1.0f, Interpolation.sine), Actions.delay(0.5f), Actions.run(new Runnable() {
						@Override
						public void run() {
							mGrowingState = GrowingState.DIG2;
					        showel.createParticle("useArea", true);
					        showel.setPositionXY(395, 228);
					        showel.setTouchable(Touchable.enabled);
						}
					})));
                }
                else if (mGrowingState == GrowingState.DIG2)
                {
                	showel.clearParticles();
                	showel.setTouchable(Touchable.disabled);
                	showel.setPosition(230, 300);
                	
                	ground_piece2.addAction(Actions.sequence(Actions.delay(1.0f), Actions.moveTo(265, 170, 0.5f, Interpolation.sine)));
                	showel.addAction(Actions.sequence(Actions.moveTo(230, 200, 1.0f, Interpolation.sine), Actions.delay(0.5f), Actions.run(new Runnable() {
						@Override
						public void run() {
							mGrowingState = GrowingState.DIG3;
					        showel.createParticle("useArea", true);
					        showel.setPositionXY(395, 228);
					        showel.setTouchable(Touchable.enabled);
						}
					})));                	
                }
                else if (mGrowingState == GrowingState.DIG3)
                {
                	showel.clearParticles();
                	showel.setTouchable(Touchable.disabled);
                	showel.setPosition(230, 300);
                	
                	ground_piece3.addAction(Actions.sequence(Actions.delay(1.0f), Actions.moveTo(187, 160, 0.5f, Interpolation.sine)));
                	showel.addAction(Actions.sequence(Actions.moveTo(230, 200, 1.0f, Interpolation.sine), Actions.delay(0.5f), Actions.run(new Runnable() {
						@Override
						public void run() {
							mGrowingState = GrowingState.SEEDS;
					        showel.addAction(Actions.fadeOut(0.5f));
					        seedsPack.addAction(Actions.fadeIn(0.5f));
					        seedsPack.setTouchable(Touchable.enabled);
					        seedsPack.createParticle("useArea", true);
						}
					})));
                }

                return true;
            }
        };
        
        showel = new GameObject("showel", false, true);
        showel.setPositionXY(395, 228);
        showel.setOrigin(42, 81);
        showel.folderName = "growFlowers";
        showel.addListener(showelClickListener);
        rootGroup.addActor(showel);
        // -------------------------------------------
        
        // ---------------SEEDS----------------------
        ClickListener seedsClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                if (mGrowingState == GrowingState.SEEDS)
                {
                	final Runnable seedsCallback = new Runnable() {
						@Override
						public void run() {
							mGrowingState = GrowingState.RAKE1;
							seedsPack.addAction(Actions.fadeOut(0.5f));
							rake.addAction(Actions.fadeIn(0.5f));
							rake.setTouchable(Touchable.enabled);
							rake.createParticle("useArea", true);
						}
					};

					final Runnable packCallback = new Runnable() {
						@Override
						public void run() {
							seeds.remove();
							rootGroup.addActor(seeds);
							seeds.setPosition(seedsPack.getPositionX() + 15, seedsPack.getPositionY() + 4);
							seeds.setOrigin(19, 33);
							seeds.setRotation(170);
							seeds.addAction(Actions.sequence(Actions.moveBy(20, -140, 0.5f), Actions.run(seedsCallback)));
							seeds.addAction(Actions.sequence(Actions.rotateBy(90, 0.5f), Actions.fadeOut(0.5f)));
						}
					};
                	
                	seedsPack.clearParticles();
                	seedsPack.setTouchable(Touchable.disabled);

                	seedsPack.addAction(Actions.rotateBy(170, 1.0f));
                	seedsPack.addAction(Actions.sequence(Actions.moveTo(220, 300, 1.0f, Interpolation.sine), Actions.run(packCallback)));
                }

                return true;
            }
        };
        
        seedsPack = new GameObject("seeds-pack", false, true);
        seedsPack.setPositionXY(412, 229);
        seedsPack.folderName = "growFlowers";
        seedsPack.setOrigin(34, 37);
        seedsPack.addListener(seedsClickListener);
        seedsPack.getColor().a = 0;
        rootGroup.addActor(seedsPack);
        
        seeds = new GameObject("seeds", false, true);
        seeds.setPositionXY(15, 4);
        seeds.folderName = "growFlowers";
        seeds.setOrigin(20, 23);
        seedsPack.addActor(seeds);
        // -------------------------------------------

        // ----------GROUND-PIECE1---------------
        ground_piece1 = new GameObject("ground-piece1", false, true);
        ground_piece1.setPositionXY(222, 188);
        ground_piece1.folderName = "growFlowers";
        rootGroup.addActor(ground_piece1);
        // -------------------------------------------

        // ---------------FLOWER----------------------
        flower = new GameObject("flower", false, true);
        flower.setPositionXY(189, 189);
        flower.setOrigin(61, 8);
        flower.folderName = "growFlowers";
        rootGroup.addActor(flower);
        // -------------------------------------------

        // ----------GROUND-PIECE2---------------
        ground_piece2 = new GameObject("ground-piece2", false, true);
        ground_piece2.setPositionXY(256, 178);
        ground_piece2.folderName = "growFlowers";
        rootGroup.addActor(ground_piece2);
        // -------------------------------------------
        
        // ----------GROUND-PIECE3---------------
        ground_piece3 = new GameObject("ground-piece3", false, true);
        ground_piece3.setPositionXY(207, 169);
        ground_piece3.folderName = "growFlowers";
        rootGroup.addActor(ground_piece3);
        // -------------------------------------------
        
        // ---------------RAKE----------------------
        ClickListener rakeClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                if (mGrowingState == GrowingState.RAKE1)
                {
                	rake.clearParticles();
                	rake.setTouchable(Touchable.disabled);
                	rake.setPosition(180, 160);
                	rake.setRotation(-60);
                	
                	ground_piece1.addAction(Actions.sequence(Actions.delay(1.0f), Actions.moveTo(222, 188, 0.5f, Interpolation.sine)));
                	rake.addAction(Actions.sequence(Actions.moveTo(300, 200, 1.0f, Interpolation.sine), Actions.delay(0.5f), Actions.run(new Runnable() {
						@Override
						public void run() {
							mGrowingState = GrowingState.RAKE2;
							rake.createParticle("useArea", true);
							rake.setPositionXY(405, 228);
							rake.setTouchable(Touchable.enabled);
							rake.setRotation(0);
						}
					})));
                }
                else if (mGrowingState == GrowingState.RAKE2)
                {
                	rake.clearParticles();
                	rake.setTouchable(Touchable.disabled);
                	rake.setPosition(180, 160);
                	rake.setRotation(-60);
                	
                	ground_piece2.addAction(Actions.sequence(Actions.delay(1.0f), Actions.moveTo(256, 178, 0.5f, Interpolation.sine)));
                	rake.addAction(Actions.sequence(Actions.moveTo(300, 200, 1.0f, Interpolation.sine), Actions.delay(0.5f), Actions.run(new Runnable() {
						@Override
						public void run() {
							mGrowingState = GrowingState.RAKE3;
							rake.createParticle("useArea", true);
							rake.setPositionXY(405, 228);
							rake.setTouchable(Touchable.enabled);
							rake.setRotation(0);
						}
					})));
                }
                else if (mGrowingState == GrowingState.RAKE3)
                {
                	rake.clearParticles();
                	rake.setTouchable(Touchable.disabled);
                	rake.setPosition(180, 160);
                	rake.setRotation(-60);
                	
                	ground_piece3.addAction(Actions.sequence(Actions.delay(1.0f), Actions.moveTo(207, 169, 0.5f, Interpolation.sine)));
                	rake.addAction(Actions.sequence(Actions.moveTo(300, 200, 1.0f, Interpolation.sine), Actions.delay(0.5f), Actions.run(new Runnable() {
						@Override
						public void run() {
							mGrowingState = GrowingState.WATER;
							rake.addAction(Actions.fadeOut(0.5f));
							water.addAction(Actions.fadeIn(0.5f));
							water.setTouchable(Touchable.enabled);
							water.createParticle("useArea", true);
						}
					})));
                }

                return true;
            }
        };
        
        rake = new GameObject("rake", false, true);
        rake.setPositionXY(405, 228);
        rake.setOrigin(38, 27);
        rake.folderName = "growFlowers";
        rake.addListener(rakeClickListener);
        rake.getColor().a = 0;
        rootGroup.addActor(rake);
        // -------------------------------------------

        // ---------------WATER----------------------
        ClickListener waterClickListener = new ClickListener() 
        {
        	private boolean isDragging = false;
        	private final Vector2 dragVec = new Vector2();
        	private final Vector2 waterCenter = new Vector2();
        	
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) 
            {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                if (mGrowingState == GrowingState.WATER)
                {
	                isDragging = true;
	                water.setRotation(60);
	                water.clearParticles();
	                
	    			dragVec.x = Gdx.input.getX();
	    			dragVec.y = Gdx.input.getY();
	    			FlowerMakerGame.gameStage.screenToStageCoordinates(dragVec);
	    			water.setPosition(dragVec.x - 75, dragVec.y - 51);

	                waterEffect.setPositionXY(water.getX() + 13, water.getY() + 13);
	                waterEffect.createParticle("water-small", true);
	                
	                flower.createParticle("useArea", true);
	                
	                dragOverTxt.addAction(Actions.fadeIn(0.25f));
                }
                
                return true;
            }
            
        	@Override
        	public void touchDragged(InputEvent event, float x, float y, int pointer) 
        	{
        		super.touchDragged(event, x, y, pointer);
        		
        		if (isDragging)
        		{
        			dragVec.x = Gdx.input.getX();
        			dragVec.y = Gdx.input.getY();
        			FlowerMakerGame.gameStage.screenToStageCoordinates(dragVec);
        			water.setPosition(dragVec.x - 75, dragVec.y - 51);
        			waterEffect.setPositionXY(water.getX() + 13, water.getY() + 13);
        			
        			waterCenter.x = event.getTarget().getX() + event.getTarget().getWidth() / 2;
        			waterCenter.y = event.getTarget().getY() + event.getTarget().getHeight() / 2;
        			if (waterCenter.x > 140 && waterCenter.x < 400 && waterCenter.y > 100 && waterCenter.y < 600)
        			{
        				flower.setScale(flower.getScaleX() + 0.002f);
        			}
        			
        			if (flower.getScaleX() >= 0.5f)
        			{
        				flower.setScale(0.5f);
        				mGrowingState = GrowingState.FERTILIZER;
        				
            			waterEffect.clearParticles();
            			flower.clearParticles();
            			water.addAction(Actions.fadeOut(0.5f));
            			fertilizer.addAction(Actions.fadeIn(0.5f));
            			fertilizer.setTouchable(Touchable.enabled);
            			fertilizer.createParticle("useArea", true);
            			
            			isDragging = false;
            			dragOverTxt.addAction(Actions.fadeOut(0.25f));
            			
            			flower.addAction(Actions.sequence(Actions.scaleTo(0.65f, 0.7f, 0.5f, Interpolation.sine), Actions.run(new Runnable() {
							@Override
							public void run() {
								flower.particleStack.get("starsBoom").start();								
							}
						}), Actions.scaleTo(0.5f, 0.5f, 0.75f, Interpolation.sine)));
            			flower.createParticle("starsBoom", true);
            			flower.loadParticleData("starsBoom");
        			}
        		}
        	}
        
        	@Override
        	public void touchUp(InputEvent event, float x, float y, int pointer, int button) 
        	{
        		super.touchUp(event, x, y, pointer, button);
        		
        		if (isDragging)
        		{
        			isDragging = false;
        			
        			water.setPositionXY(331, 229);
        			water.setRotation(0);
        			water.createParticle("useArea", true);
        			
        			waterEffect.clearParticles();
        			flower.clearParticles();
        			
        			dragOverTxt.addAction(Actions.fadeOut(0.25f));
        		}
        	}
        };
        
        water = new GameObject("water", false, true);
        water.setPositionXY(331, 229);
        water.folderName = "growFlowers";
        water.setOrigin(75, 51);
        water.addListener(waterClickListener);
        water.getColor().a = 0;
        rootGroup.addActor(water);

        waterEffect = new GameObject("waterEffect", false, true);
        waterEffect.setPositionXY(water.getX() + 16, water.getY() + 89);
        waterEffect.folderName = "growFlowers";
        rootGroup.addActor(waterEffect);
        
        dragOverTxt = new TextGameObject("dragOverTxt", false, "SegoePrint-32-Shadow", 32);
        dragOverTxt.getColor().a = 0;
        dragOverTxt.setTextCenteredOnPosition(Const.TXT_DRAG_OVER_FLOWER, 240, 500);
        rootGroup.addActor(dragOverTxt);
        // -------------------------------------------

        // ---------------FERTILIZER------------------
        ClickListener fertilizerClickListener = new ClickListener() 
        {
        	private boolean isDragging = false;
        	private final Vector2 dragVec = new Vector2();
        	private final Vector2 fertilizerCenter = new Vector2();
        	
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) 
            {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                if (mGrowingState == GrowingState.FERTILIZER)
                {
	                isDragging = true;
	                fertilizer.setRotation(180);
	                fertilizer.clearParticles();
	                
	    			dragVec.x = Gdx.input.getX();
	    			dragVec.y = Gdx.input.getY();
	    			FlowerMakerGame.gameStage.screenToStageCoordinates(dragVec);
	    			fertilizer.setPosition(dragVec.x - fertilizer.getOriginX(), dragVec.y - fertilizer.getOriginY());

	    			fertilizerEffect.setPositionXY(fertilizer.getX() + 47, fertilizer.getY() + 7);
	    			fertilizerEffect.createParticle("dust-small", true);
	                
	                flower.createParticle("useArea", true);
	                
	                dragOverTxt2.addAction(Actions.fadeIn(0.25f));
                }
                
                return true;
            }
            
        	@Override
        	public void touchDragged(InputEvent event, float x, float y, int pointer) 
        	{
        		super.touchDragged(event, x, y, pointer);
        		
        		if (isDragging)
        		{
        			dragVec.x = Gdx.input.getX();
        			dragVec.y = Gdx.input.getY();
        			FlowerMakerGame.gameStage.screenToStageCoordinates(dragVec);
        			fertilizer.setPosition(dragVec.x - fertilizer.getOriginX() + rand.nextInt(5) - 2, dragVec.y - fertilizer.getOriginY() + rand.nextInt(5) - 2);
        			fertilizerEffect.setPositionXY(fertilizer.getX() + 47, fertilizer.getY() + 7);
        			
        			fertilizerCenter.x = event.getTarget().getX() + event.getTarget().getWidth() / 2;
        			fertilizerCenter.y = event.getTarget().getY() + event.getTarget().getHeight() / 2;
        			if (fertilizerCenter.x > 140 && fertilizerCenter.x < 400 && fertilizerCenter.y > 100 && fertilizerCenter.y < 600)
        			{
        				flower.setScale(flower.getScaleX() + 0.002f);
        			}
        			
        			if (flower.getScaleX() >= 1.0f)
        			{
        				flower.setScale(1.0f);
        				mGrowingState = GrowingState.FINISHED;
        				
        				fertilizerEffect.clearParticles();
            			flower.clearParticles();
            			fertilizer.addAction(Actions.fadeOut(0.5f));
            			
            			next_btn.addAction(Actions.fadeIn(0.5f));
            			next_btn.setTouchable(Touchable.enabled);
            			next_btn.createParticle("useArea", true);
            			next_btn.setVisible(true);
            			next_btn.addAction(Actions.sequence(Actions.scaleTo(1.2f, 1.2f, 0.25f, Interpolation.sine), Actions.scaleTo(1.0f, 1.0f, 0.25f, Interpolation.sine)));
            			
            			isDragging = false;
            			dragOverTxt2.addAction(Actions.fadeOut(0.25f));
            			
            			flower.addAction(Actions.sequence(Actions.scaleTo(1.15f, 1.2f, 0.5f, Interpolation.sine), Actions.run(new Runnable() {
							@Override
							public void run() {
								flower.particleStack.get("starsBoom").start();								
							}
						}), Actions.scaleTo(1.0f, 1.0f, 0.75f, Interpolation.sine)));
            			flower.createParticle("starsBoom", true);
            			flower.loadParticleData("starsBoom");
        			}
        		}
        	}
        
        	@Override
        	public void touchUp(InputEvent event, float x, float y, int pointer, int button) 
        	{
        		super.touchUp(event, x, y, pointer, button);
        		
        		if (isDragging)
        		{
        			isDragging = false;
        			
        			fertilizer.setPositionXY(355, 229);
        			fertilizer.setRotation(0);
        			fertilizer.createParticle("useArea", true);
        			
        			fertilizerEffect.clearParticles();
        			flower.clearParticles();
        			
        			dragOverTxt2.addAction(Actions.fadeOut(0.25f));
        		}
        	}
        };
        
        fertilizer = new GameObject("fertilizer", false, true);
        fertilizer.setPositionXY(355, 229);
        fertilizer.folderName = "growFlowers";
        fertilizer.setOrigin(58, 52);
        fertilizer.addListener(fertilizerClickListener);
        fertilizer.getColor().a = 0;
        rootGroup.addActor(fertilizer);
        
        fertilizerEffect = new GameObject("fertilizerEffect", false, true);
        fertilizerEffect.setPositionXY(fertilizer.getX() + 47, fertilizer.getY() + 7);
        fertilizerEffect.folderName = "growFlowers";
        rootGroup.addActor(fertilizerEffect);
        
        dragOverTxt2 = new TextGameObject("dragOverTxt2", false, "SegoePrint-32-Shadow", 32);
        dragOverTxt2.getColor().a = 0;
        dragOverTxt2.setTextCenteredOnPosition(Const.TXT_DRAG_OVER_FLOWER2, 240, 500);
        rootGroup.addActor(dragOverTxt2);        
        // -------------------------------------------

        // ----------BACK-BTN---------------
        ClickListener backClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene(getPrevScreenName());

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        back_btn = new GameObject("back-btn", false, true);
        back_btn.setPositionXY(15, 16);
        back_btn.folderName = "main";
        back_btn.setTouchable(Touchable.enabled);
        back_btn.addListener(backClickListener);
        rootGroup.addActor(back_btn);
        // -------------------------------------------
        
        // ----------NEXT-BTN---------------
        ClickListener nextClickListener = new ClickListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AudioPlayer.playSound("buttonSound01", 0.25f);

                rootGroup.setTouchable(Touchable.disabled);
                FlowerMakerGame.mScreenChangeHelper.changeScene("protectMg");

                event.getTarget().clearActions();
                event.getTarget().addAction(Actions.sequence(Actions.color(new Color(0.6f, 0.6f, 0.6f, 1.0f), Constants.BUTTONS_PRESS_TIME), Actions.color(new Color(1.0f, 1.0f, 1.0f, 1.0f), Constants.BUTTONS_PRESS_TIME)));

                return true;
            }
        };
        
        next_btn = new GameObject("next-btn", false, true);
        next_btn.setPositionXY(355, 16);
        next_btn.setOrigin(55, 39);
        next_btn.folderName = "main";
        next_btn.setTouchable(Touchable.enabled);
        next_btn.addListener(nextClickListener);
        next_btn.setVisible(false);
        rootGroup.addActor(next_btn);
        // -------------------------------------------
        
        FlowerMakerGame.gameStage.addActor(rootGroup);
    }
    
    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show(){
        if (mActionResolver != null) {
            mActionResolver.showRateForPlays(new ActionResolver.IOnRate() {
              @Override
              public void onRate() {

              }
            });
        }
        
        super.show();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
    	super.resume();
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPreUnLoad() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScenePlaced() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void resetVariables() {
    	back_btn.clearActions();
    	back_btn.setColor(1, 1, 1, 1);
    	
    	next_btn.clearActions();
    	next_btn.clearParticles();
    	next_btn.setColor(1, 1, 1, 1);
    	next_btn.setVisible(false);
    	
    	mGrowingState = GrowingState.DIG1;
    	
    	ground_piece1.setPositionXY(222, 188);
    	ground_piece2.setPositionXY(256, 178);
    	ground_piece3.setPositionXY(207, 169);
        flower.setScale(0);
        
        showel.createParticle("useArea", true);
        showel.setPositionXY(395, 228);
        showel.getColor().a = 1;
        showel.setTouchable(Touchable.enabled);
        
        seedsPack.getColor().a = 0;
        seedsPack.setPositionXY(412, 229);
        seedsPack.clearParticles();
        seedsPack.setTouchable(Touchable.disabled);
        seedsPack.setRotation(0);
        seeds.setPositionXY(15, 4);
        seeds.setRotation(0);
        seeds.getColor().a = 1;
        seedsPack.addActor(seeds);

        
        rake.getColor().a = 0;
        rake.setPositionXY(405, 228);
        rake.clearParticles();
        rake.setTouchable(Touchable.disabled);
        rake.setRotation(0);
        
        water.getColor().a = 0;
        water.setPositionXY(331, 229);
        water.clearParticles();
        water.setRotation(0);
        water.setTouchable(Touchable.disabled);
        
        fertilizer.getColor().a = 0;
        fertilizer.setPositionXY(355, 229);
        fertilizer.clearParticles();
        fertilizer.setRotation(0);
        fertilizer.setTouchable(Touchable.disabled);
        
        dragOverTxt.setColor(1, 1, 1, 0);
        dragOverTxt2.setColor(1, 1, 1, 0);
    }

    @Override
    protected String getPrevScreenName() {
        return "mainMenu";
    }
}
