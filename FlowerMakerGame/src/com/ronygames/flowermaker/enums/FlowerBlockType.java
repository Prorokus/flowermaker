package com.ronygames.flowermaker.enums;

import com.casualWorkshop.callbacks.BlockObjectType;

/**
 * Created by VLaD on 06.03.14.
 */
public enum FlowerBlockType implements BlockObjectType<FlowerBlockType> {
    FREE(BlockObjectType.FREE),
    IS_FLOWER(0),
    IS_DECORATIONS(1),
    IS_FULL(2);

    private final int id;

    FlowerBlockType(int id) {
        this.id = id;
    }

    @Override
    public int getId(){
        return id;
    }

    @Override
    public FlowerBlockType getType(){
        return this;
    }
}
